<?php

// on ne change qu'un tout petit truc
include_spip('ecrire/public/compiler');

// mais on charge aussi l'autoloader de Slex
include_spip('slex/Spip/Autoloader');
Spip\Autoloader::register();

// surcharge pour pas avoir des doubles échappements avec Slex !
function public_compiler($squelette, $nom, $gram, $sourcefile, $connect=''){
	// Pour le moment on teste juste avec la grammaire .slex
	// notre nouveau jouet.
	//
	// Mais on est obligé de surcharger compiler()
	// car là c'est lui qui échappe les caractères au lieu du phraseur_html
	// et c'est un peu beaucoup nul, puisque notre phraseur slex s'en charge
	// de manière propre et native.
	if ('.slex.html' != substr($sourcefile, -10)) {
		return public_compiler_dist($squelette, $nom, $gram, $sourcefile, $connect='');
	}

	$gram = 'slex';
	# pour debug ?gram=html | gram=slex
	if (_request('gram')) $gram = _request('gram');

	// Surcharge donc :
	// en gros, c'est la même chose sans le code pour les échappements.
	// -----------------

	// Pre-traitement : reperer le charset du squelette, et le convertir
	// Bonus : supprime le BOM
	include_spip('inc/charsets');
	$squelette = transcoder_page($squelette);

	// rendre inertes les echappements de #[](){}<>
	$descr = array('nom' => $nom,
			'gram' => $gram,
			'sourcefile' => $sourcefile,
			'squelette' => $squelette);

	// Phraser le squelette, selon sa grammaire
	$boucles = array();
	$f = charger_fonction('phraser_' . $gram, 'public');
	$squelette = $f($squelette, '', $boucles, $descr);
#var_dump($squelette);
	$boucles = compiler_squelette($squelette, $boucles, $nom, $descr, $sourcefile, $connect);

	$debug = ($boucles AND defined('_VAR_MODE') AND _VAR_MODE == 'debug');
	if ($debug) {
		include_spip('public/decompiler');
		foreach($boucles as $id => $boucle) {
			if ($id)
			  $decomp = "\n/* BOUCLE " .
			    $boucle->type_requete .
			    " " .
			    str_replace('*/', '* /', public_decompiler($boucle, $gram, 0, 'criteres')) .
			    " */\n";
			else $decomp = ("\n/*\n" . 
				 str_replace('*/', '* /', public_decompiler($squelette, $gram)) 
				 . "\n*/");
			$boucles[$id]->return = $decomp .$boucle->return; 
			$GLOBALS['debug_objets']['code'][$nom.$id] = $boucle->return;
		}
	}

	return $boucles;
}
