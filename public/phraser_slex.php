<?php

# temporaire (évite une notice sur TYPE_RECURSIF - au moins)
include_spip('public/phraser_html');

/**
 * Phrasage d'une extension .slex
 *
 * @param string $texte Code source du squelette
 * @param string $id_parent Inutilisé
 * @param array  $boucles AST
 * @param array  $descr Descriptions du fichier (nom, gram, sourcefile, squelette)
 * @return \Spip\Compilo\Ast AST sous forme d'iterateur
**/
function public_phraser_slex_dist($texte, $id_parent, &$boucles, $descr, $ligne=1) {
	static $lexer = null;
	static $parser = null;

	if (is_null($lexer)) {
		$lexer  = new Spip\Compilo\Lexer\Spico();
		$parser = new Spip\Compilo\Parser();
	}

	// iterateur de lexemes (tokens)
	$stream = $lexer->tokenize($texte, $descr['sourcefile']);
	// l'ast de code
	$ast = $parser->parse($stream);
	// la liste des boucles à plat par identifiant ('id_boucle' => Boucle)
	$boucles = $ast->getBoucles();

	return $ast->getAst();
}
