
Plugin Slex pour SPIP
=====================

Slex, pour «SPIP Lexeur», est un plugin SPIP offrant un lexeur et un parseur
pour SPIP.

Un lexeur est un outil qui découpe une syntaxe en éléments identifiables
(des lexèmes ou tokens). Un parseur analyse ces lexèmes pour créer un arbre
de syntaxe abstrait (AST). L'AST, qui décrit sémantiquement le langage, est
ensuite utilisé par la suite du compilateur qui comprendra ce qui est demandé,
vérifiera que tout est possible et transformera la demande dans un autre langage
(PHP en ce qui concerne SPIP)

SPIP, dans «plugins/phraseur.php» dispose d'un parseur qui transforme le code
SPIP en AST, en s'affranchissant du découpage en lexèmes préalable
(disons plutôt qu'il fait les 2 en même temps, pas l'un après l'autre).


Étapes dans Slex
----------------

Dans Slex, inspiré de Twig, ces deux étapes sont séparées :

- le Lexeur découpe d'abord le code source d'un squelette en lexèmes (tokens).
  Il retourne une suite (un flux) de lexèmes (TokenStream). C'est une liste
  ordonnée de lexèmes, d'éléments de syntaxe que l'on connait. Cela s'approche
  de l'équivalent en PHP «token_get_all()».
  Le lexeur peut générer des erreurs de syntaxe dans certaines circonstances.

- Le parseur prend ensuite ce flux de lexèmes et le traduit dans un Ast.
  C'est le parseur qui analyse le flux et crée un arbre. Il sait que
  tel élément peut être dans un autre, tel autre pas.
  Il peut donc lui aussi générer des erreurs de syntaxe.
  Il sort un Ast qui peut être compris par un compilateur,
  typiquement celui de SPIP.


Ce qui est permis par Slex
--------------------------

Slex permet, de part ce découpage différentes choses :

- d'avoir en entrée des syntaxes différentes, qui, si le lexeur
  crée un flux de tokens compris par le parseur peuvent être traitées.

  Ici, nous avons 2 syntaxes possibles en entrée :
  
  * `html`  : est la syntaxe native, d'origine de SPIP (SPIP <= 3.*)
  * `spico` : est une syntaxe étendue (la même avec plus de choses)


- d'avoir en sortie des AST différents.

  Ici, nous avons 2 AST possibles en sortie :

  * `original` : est l'AST attendu par le compilateur de SPIP. l'AST natif,
                 avec cependant 2 choses utiles supplémentaires :
                `echo $ast;`, et `$ast->toXML();`
  * `spici`    : est un AST beaucoup plus proche de celui que crée Twig dans son
                 organisation, mais inutilisable avec le compilateur de SPIP
                 évidemment (inutilisable avec Twig non plus d'ailleurs, ce n'est pas le but).


Spico / Spici
-------------

C'était juste pour donner des noms distinctif pour être clair entre les
différentes fonctions de ce plugin. Ces mots ont une origine d'esperanto :

- Spico signifie «Épice», et se prononce «spitso»
- Spici signifie «Assaisonner, Épicer», et se prononce «spitsi».
- Spica signifie «Piquant, Épicé», et se prononce «spitsa».


Organisation des répertoires de SLex
------------------------------------

À la racine, on retrouve tout ce qui concerne un plugin SPIP habituellement
avec une surcharge actuellement de public/compiler pour prendre en compte
Slex et notre lexeur/parseur.

Le répertoire «slex» est ce lexeur/parseur. Ce qui est dedans est INDÉPENDANT
de SPIP et peut être utilisé tel quel.

Il se compose de ces répertoires :

- docs : documentations 
- test : tests unitaires au format PHP-Unit
- Spip : les sources du lexeur/parseur

Les autres répertoires ne sont là que pour tester temporairement
(img, js, css et index.php)

Se rapporter au répertoire docs de slex pour en apprendre encore plus !

