<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'slex_nom' => 'Slex',
	'slex_slogan' => 'Un lexer qui booste tout !',
	'slex_description' => 'Ce plugin offre un lexeur et phraseur pour SPIP
		différent de celui fournit par par défaut.',
);

?>
