<?php

// utilitaires pour les tests utilisés par index.php


function slex_debut_html($titre = "Dev d'un nouveau lexer") {
return <<<EOF
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>$titre</title>
		<link rel='stylesheet' href="slex/css/bootstrap.min.css" />
		<link rel='stylesheet' href="slex/css/slex.css" />
		<link rel='stylesheet' href="slex/css/spip_tokens.css" />
		<script type='text/javascript' src="slex/js/jquery.js"></script>
		<script type='text/javascript' src="slex/js/bootstrap.min.js"></script>
	</head>
	<body>
		<h1>$titre</h1>
EOF;
}


function slex_fin_html($titre = "Dev d'un nouveau lexer") {
return <<<EOF
	</body>
</html>
EOF;
}


function slex_parties_html($source) {
$t = '<div class="tabbable tabs-left">';
	$t .= '<ul class="nav nav-tabs">';
	foreach (array('Html', 'Spico') as $i => $type_lexer) {
		$tabid = 'tab'.md5($type_lexer);
		$active = (($i == 0) ? " class='active'" : '');
		$t .= "<li$active><a href='#$tabid' data-toggle='tab'>Lexeur $type_lexer</a></li>";
	}
	$t .=  '</ul>';
	$t .=  '<div class="tab-content">';
	foreach (array('Html', 'Spico') as $i => $type_lexer) {
		$tabid = 'tab'.md5($type_lexer);
		$active = (($i == 0) ? " active" : '');
		$t .= "<div class='tab-pane$active' id='$tabid'>";
			$t .= slex_contenu_html($type_lexer, $source);
		$t .=  '</div>';
	}
	$t .= '</div>';
$t .= '</div>';
return $t;
}


function slex_contenu_html($type_lexer, $source) {
	$lexer  = Spip\Compilo\Lexer::Lexer($type_lexer);
	$tokens = $lexer->tokenize($source);
	$parser  = new Spip\Compilo\Parser();



	// ---------
	// affichage
	$id = md5($type_lexer);

$t = <<<EOF
	<ul class="nav nav-tabs" id="elements_$id">
		<li class="active"><a href="#code_$id" data-toggle="tab">Code</a></li>
		<li><a href="#tokens_$id" data-toggle="tab">Tokens</a></li>
		<li><a href="#AST_Original_$id" data-toggle="tab">AST Original</a></li>
		<li><a href="#AST_Spici_$id" data-toggle="tab">AST Spici</a></li>
	</ul>

	<div class="tab-content">
		<div class="tab-pane active" id="code_$id">
EOF;

	$t .= slex_accordion('Source',               htmlspecialchars($source));
	$t .= slex_accordion('Colorieur de tokens',  Spip\Helper\TokenStreamViewer::html($tokens), true, false);

	$t .= "</div>";
	$t .= "<div class='tab-pane' id='tokens_$id'>";

	$t .= slex_accordion('Echo des tokens',      $tokens);
	$t .= slex_accordion('Print des tokens',     print_r($tokens, true), false); # caché par défaut

	$t .= "</div>";

	foreach (array('Original', 'Spici') as $j => $type_ast) {

		$tokens->rewind();
		$ast     = $parser->parse($tokens, Spip\Compilo\Ast::Ast($type_ast));
		$boucles = $ast->getBoucles();
		$ast     = $ast->getAst();

		$t .= "<div class='tab-pane' id='AST_${type_ast}_$id'>";

		$t .= slex_accordion('Echo de l’AST',        $ast);
		$t .= slex_accordion('Print de l’AST',       print_r($ast, true), false); # caché par défaut

		$foreach = '';
		foreach ($ast as $i => $node) {
			$foreach .= "\n#$i => \n" . print_r($node, true);
		}

		$t .= slex_accordion('Foreach de l’AST',     $foreach, false);
		$t .= slex_accordion('XML de l’AST',         htmlentities($ast->toXML()));

		$t .= "</div>";
	}

	$t .= "</div>";

	return $t;
}




function slex_accordion($titre, $code, $open=true, $pre = true) {
	$id = uniqid();
	$plus = '';
	if (!$open and $code) $plus = '  [ + ]';
	$t = "<h4 data-toggle='collapse' data-target='#$id'>$titre$plus</h4>\n";
	$t .= "\n<div id='$id' class='collapse " . ($open ? 'in' : ''). "'>\n";
		$t .= ($pre ? "<pre>" : '');
		$t .= $code;
		$t .= ($pre ? "</pre>\n" : '');
	$t .= "</div>\n";
	return $t;
}

