<?php

# afficher toutes les erreurs
error_reporting(E_ALL);
ini_set("display_errors", "On");

# autoloader de classes SPIP
require_once 'slex/Spip/Autoloader.php';
Spip\Autoloader::register();


$codes = array(
	'texte_simple'        => "un texte simple",
	'texte_retour'        => "un texte simple\n et une ligne",
	'commentaire'         => "[% commentaire %]",
	'commentaire_doc'     => "[%% autre commentaire %]",
	'commentaires'        => "[% autre %][% commentaire %]",
	'texte_commentaire'   => "un [% commentaire %] et une fin",
	'double_commentaires' => "un [% commentaire %] deux [% commentaires %]",
	'echappement'         => "un \[",
	'echappements'        => "un \[ crochet \] dedans",
	'multi'               => "<multi> du texte [en] en anglais [fr] en français</multi>",
	'multi_texte'         => "Texte avant\n<multi> du texte [en] en anglais [fr] en français</multi>",
	# 1 seule chaine de langue avec le phraseur de SPIP 3 si un [ est présent
	'multi_crochet'       => "<multi>[en]Texte [ texte [fr]Texte apres</multi>",
	'multi_commentaire'   => "<multi>[en]Text[% [fr]Texte %] apres[eo]Teksto</multi>",
	'multi_echappement'   => "<multi>[en]Text \[fr\] apres[eo]Teksto</multi>",
	'multi_ln'            => "Un texte simple avec multi\n\n<multi>\nTexte\n[en] Text\n[fr] Tyxte\n[eo] Texto\n</multi>\n",
	'multi_double'        => "<multi>[fr]Texte1</multi> entre <multi>[fr]Texte2</multi>",

	'idiomme'             => '<:espace_prive:>',
	'idiomme_module'      => '<:spip:espace_prive:>',
	'idiomme_double'      => '<:idiome1:><:idiome2:>',
	'idiomme_double_t'    => '<:spip:espace_prive:> entre <:spip:espace_prive:>',
);

$_source = isset($_REQUEST['source']) ? $_REQUEST['source'] : 'texte_simple';
if (isset($codes[$_source])) {
	$source = $codes[$_source];
} else {
	$source = $codes['texte_simple'];
}


include_once __DIR__ . '/utilitaires_test.php';

echo slex_debut_html();
echo slex_parties_html($source);

echo "<hr class='separateur' />";
echo "<h5>Codes sources possibles</h5>";

$i = 0;
echo "<div class='row'>";
echo "<ul class='span2'>";
foreach($codes as $nom => $code) {
	$i++;
	$url = $_SERVER["PHP_SELF"] . "?source=$nom";
	if ($_source == $nom) {
		echo "<li class='active'>$nom</li>";
	} else {
		echo "<li><a href='$url'>$nom</a></li>";
	}
	if ($i and ($i % 5 == 0)) {
		echo "</ul><ul class='span2'>";
	}
}
if ($i % 5) {
	echo "</ul>";
}
echo "</div>";

echo slex_fin_html();


