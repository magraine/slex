Notes
=====

- balises :      voir elements/balises.md
- commentaires : voir elements/commentaires.md
- échappements : voir elements/echappements.md



Sur les idiomes
---------------

Pas d'idiome dans un idiome : `<:ok:>` mais pas `<:ok{<:ok:>}:>`


Sur les polyglottes
-------------------

Pas de multi dans un multi.
Il n'y a que du texte et des déclarations de langue dans un multi.

Des commentaires peuvent être mis sur du texte, mais pas dans
une déclaration de langue.


Les cas tordus
--------------

- `[ texte [ texte (#BALISE)]`
- `[ 'texte [ texte' (#BALISE)]`
- `[ 'texte [ 'texte' (#BALISE)]`

=> Le premier crochet non échappé et valide trouvé, au plus près de la balise.
   Ici donc, toujours le crochet intérieur dans les exemples.


- `<multi>[fr] Texte [ </multi> (#BALISE)]`
- `[ <multi>[fr] Texte [ </multi> (#BALISE)]`

=> Lecture de gauche à droite dans l'analyse.
   Comme on tombre d'abord sur le multi, le crochet
   à l'intérieur du multi est considéré comme du texte.

   De même si le multi a un crochet de balise à sa gauche :
   ces crochets ne sont traités comme élément qu'au moment du passage
   sur #BALISE, donc après que le multi ait été parsé.
   Dans le 2e exemple donc, tout le multi est dans la partie optionnelle
   de la balise.

   



