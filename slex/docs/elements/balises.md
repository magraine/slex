
Balises
=======

Les balises complètes
---------------------

Les balises complètes ont un texte optionnel avant et après la balise
qui ne s'affiche que si le contenu retourné par la balise n'est pas vide.

La syntaxe de ces balises est : `[ avant (#BALISE) après ]`

Pour qu'une balise soit complète, avec parties alternatives, il faut qu'elle
ait une parenthèse aussitôt avant le `#` : `(#BALISE`.

Corrolaire : toute balise avec une parenthèse précédent juste le # est forcément
une balise complète : `[ ... (#B...) ... ]`

Il ne peut y avoir de commentaire entre `(` et `#` pour une balise complète.

Lorsqu'une balise est complète, le premier `[` avant ou `]` après la balise
qui est libre (ie: non encore utilisé par une autre balise complète)
sera pris comme étant l'ouverture de la balise complète (ou la fermeture).

    Ainsi dans : `[1 [2 [3 (#B1) 4] (#B2) 5] 6]`

    - les crochets 3 et 4 sont ceux de la balise B1 : les plus près d'elle.
    - les crochets 2 et 5 sont ceux de B2 : les plus près libres d'elle
      (sachant que 3 est pris par B1)
    - les crochets 1 et 6 sont libres (s'ils ne sont pas utilisés par une autre balise
      ils seront considérés comme du texte).

Pour qu'un crochet ne soit pas affecté par ces recherches, il faut l'échapper

    Ainsi dans : `[1 [2 [3 (#B) ]`

    - par défaut, c'est le crochet 3 qui sera affecté à B
    - `[1 [2 \[3 (#B)]` indique que le crochet 3 est un texte,
      c'est donc le suivant, 2, qui sera affecté à B
    - `[1 \[2 \[3 (#B)]` affectera donc le crochet 1 à B.
    
