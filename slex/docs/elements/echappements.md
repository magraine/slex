

Échappements
============

Certains caractères peuvent être échappés par `\` : `[]{}()<>#`

Exemple :

    `\(#BALISE` échappera la parenthèse.
    `\(\#BALISE` affichera le texte `(#BALISE`

Les échappements permettent à des éléments ressemblant à des mots clés de
SPIP de ne pas être vus comme tels, mais comme de simples texte du
caractère échappé.
