
Commentaires
============

Un commentaire permet de ne pas prendre en compte, par le compilateur
le code déclaré entre le début et la fin du commentaire.

Ce code sera tout simplement ignoré et ne se retrouvera pas dans le code
compilé produit.

Syntaxe
-------

Un commentaire s'écrit : `[% commentaire %]`

Une ouverture de commentaire `[%` impose quelque part après l'ouverture
sa fermeture `%]`, sinon une erreur de syntaxe sera levée.

Un commentaire peut être de type documentation : `[%% est une doc %]`.
L'ouverture possède alors 2 signes `%`.

Un commentaire peut être placé dans tout élément texte, ou entre
chaque élément spécifique de SPIP dans les espaces libres, mais il ne
doit pas couper un mot clé de SPIP (sans quoi celui-ci ne sera pas reconnu).

Exemple
-------

    ```
    Corrects : 
    [ t [% commentaire %] (#BALISE)]
    [ t  (#BALISE) [% commentaire %] ]
    [ t  (#BALISE[% commentaire %])]
    [(#BALISE | [% commentaire %] filtre)  ]
    [(#BALISE [% commentaire %] | filtre)  ] 
    [(#BALISE|filtre  [% commentaire %] { arg })]
    [(#BALISE|filtre { [% commentaire %] arg })]
    [(#BALISE|filtre { arg [% commentaire %]})]
    
    Incorrects :
    [ t  ([% commentaire %]#BALISE)] : aucun espace entre ( et # d'une balise complète)
    [ t  (#BA[% commentaire %]LISE)] : pas de découpe d'un mot clé en 2
    <BOU[% c %]CLE : pas de découpe d'un mot clé en 2
    ```

