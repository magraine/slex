
Le Lexeur
=========

Objectifs et utilisation
------------------------

Son objectif est de découper en lexèmes le code source qui lui est donné.

Il vérifie également que le code source possède une syntaxe conforme au
langage, par exemple qu'un commentaire ouvert est bien fermé.

Son résultat est un flux de lexèmes (tokens) qui peut servir à différentes choses :

- au parseur qui l'analyse pour former un AST (arbre de syntaxe abstraite)
- à un colorieur pour colorer le code source en fonction de ses lexèmes.


Fonctionnement
--------------

Le lexeur commence par découper le texte avec tous les débuts d'éléments
possibles de sa syntaxe (début de commentaire, début de polyglotte, ...).

Ce résultat est mis dans le tableau `positions` qui indique, pour chaque élément,
sa position (nombre de caractères) par rapport au début du texte.

Deux pointeurs se déplacent en avançant dans le texte pour le décrire :

- un pointeur sur le texte (cursor) : la position du curseur depuis le
  début du texte, en nombre de caractères.
- un pointeur sur la position actuelle (position) : la position en
  cours d'analyse, qui est un numéro de position. Une position
  correspondant donc à un début d'un élément SPIP.

Le lexeur dispose également d'une pile d'état (states) et de l'état
courant (state) qui indique quel type de donnée est en cours d'analyse
actuellement.

Au tout début, on est sur du DATA (un texte en dehors de SPIP).

Lorsque le pointeur courant arrive à une position qui est un début
d'un élément SPIP, en fonction de l'élément, on peut entrer dans un
nouvel état d'analyse. Par exemple si on entre dans une ouverture de
polyglotte (`<multi>`), le lexeur empile un nouvel état polyglotte.

Ce nouvel état fait que l'avancement du pointeur ne se comportera pas
pareil : cet état cherchera du texte, du commentaire, des déclarations de
langue ou une fin de polyglotte. S'il rencontre une ouverture de
boucle, il émettra une erreur : elle n'a rien à faire là. Le lexeur
quittera l'état 'polyglotte' lorsqu'il verra la fin du multi. S'il ne
le voit pas, il émettra une erreur de syntaxe également.

Le lexeur gère donc une pile d'état car certains éléments peuvent
s'imbriquer : une balise dans un critère dans une boucle empilera
différents états et les dépilera au fur et à mesure de leur sortie.




