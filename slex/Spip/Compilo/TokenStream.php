<?php


/**
 * Le flux de lexèmes (tokens) est un itérateur permettant
 * de parcourir les tokens d'un code source qui ont été
 * trouvés par un lexeur.
**/

namespace Spip\Compilo;

/**
 * Flux de tokens
 */
class TokenStream implements \SeekableIterator
{
	/** Liste des tokens */
	protected $tokens;
	/** Position courante */
	protected $current;
	/** Nom du fichier (si connu) */
	protected $filename;

	/**
	 * Constructeur
	 *
	 * @param array  $tokens   Liste de tokens
	 * @param string $filename Chemin du fichier associé aux tokens
	 */
	public function __construct(array $tokens, $filename = null)
	{
		$this->tokens     = $tokens;
		$this->current    = 0;
		$this->filename   = $filename;
	}

	/**
	 * Retourne la liste sous forme de chaîne
	 * @return string
	 */
	public function __toString()
	{
		return implode("\n", $this->tokens);
	}

	/**
	 * Ajoute des tokens au niveau de la position courante du curseur
	 * @return array
	 */
	public function ajouterTokens(array $tokens)
	{
		$this->tokens = array_merge(
			array_slice($this->tokens, 0, $this->current), $tokens,
			array_slice($this->tokens, $this->current));
	}

	/**
	 * Retourne à la première position
	**/
	function rewind() {
		$this->current = 0;
	}

	/**
	 * Retourne le token courant
	 * @return Token
	 */
	public function current()
	{
		return $this->tokens[$this->current];
	}

	/**
	 * Retourne la position actuelle
	 * @return int
	 */
	function key() {
		return $this->current;
	}

	/**
	 * Avance d'un cran 
	 */
	public function next()
	{
		++$this->current;
	}

	/**
	 * Indique si la position actuelle est valide
	 * @return Token
	 */
	function valid() {
		return isset($this->tokens[$this->current]);
	}

	/**
	 * Va à l'emplacement indiqué
	 * @param int $position
	**/
	public function seek($position) {
		$this->current = $position;

		if (!$this->valid()) {
			throw new \OutOfBoundsException("Position invalide dans seek ($position)");
		}
	}

	/**
	 * Test qu'un token a la valeur indiquée, sinon retourne une exception
	 * @return Token
	 */
	public function expect($type, $value = null, $message = null)
	{
		$token = $this->tokens[$this->current];
		if (!$token->test($type, $value)) {
			$line = $token->getLine();
			throw new Error\Syntax(sprintf('%sUnexpected token "%s" of value "%s" ("%s" expected%s)',
				$message ? $message.'. ' : '',
				Token::typeToEnglish($token->getType(), $line), $token->getValue(),
				Token::typeToEnglish($type, $line), $value ? sprintf(' with value "%s"', $value) : ''),
				$line,
				$this->filename
			);
		}
		$this->next();

		return $token;
	}


	/**
	 * Regarde le prochain token.
	 *
	 * @param int $number
	 * @return Token
	 */
	public function look($number = 1)
	{
		if (!isset($this->tokens[$this->current + $number])) {
			throw new Error\Syntax('Unexpected end of template', $this->tokens[$this->current + $number - 1]->getLine(), $this->filename);
		}

		return $this->tokens[$this->current + $number];
	}

	/**
	 * Teste le token courant
	 * @return bool
	 */
	public function test($primary, $secondary = null)
	{
		return $this->tokens[$this->current]->test($primary, $secondary);
	}


	/**
	 * Teste si on arrive à la fin du flux
	 * @return bool
	 */
	public function isEOF()
	{
		return $this->tokens[$this->current]->getType() === Token::ST_EOF;
	}



	/**
	 * Retourne le fichier associé au flux
	 * @return string|null
	 */
	public function getFilename()
	{
		return $this->filename;
	}
}
