<?php

/**
 * L'AST est un arbre sémantique des données issues du parseur.
 *
 * Cette classe abstraite sert de base pour la génération de différents AST.
 * Les différentes fonctions qu'elle déclare doivent être fournies par
 * ses héritières.
**/

namespace Spip\Compilo;


/**
 * Classe abstraite d'AST
 *
 * Les différents AST possibles doivent implémenter ses fonctions abstraites
 * pour fonctionner lorsqu'elles étendent cette classe
**/
abstract class Ast
{

	/**
	 * Crée un Ast du type demandé et le retourne
	 *
	 * @param string $type
	 * @param array $options
	 * @return Lexer
	**/
	public static function Ast($type) {
		$a = "Spip\Compilo\Ast\\$type";
		return new $a();
	}

	/**
	 * Retourne l'ast
	 * @return Object
	**/
	abstract public function getAst();

	/**
	 * Retourne la liste des boucles classées par nom
	 * @return array()
	**/
	abstract public function getBoucles();

	/**
	 * Défini les enfants racine de l'AST
	 * @param array $liste Liste des premiers enfants
	 */
	abstract public function setNodes($liste);



	/**
	 * Retourne un noeud de Texte
	 * 
	 * @param string $texte Texte du texte
	 * @param string $avant Possible élément avant (apostrophe ?)
	 * @param string $apres Possible élément après (apostrophe ?)
	 * @param int $ligne Numéro de ligne
	 * @return Object
	**/
	abstract public function Texte($texte, $avant, $apres, $ligne);



	/**
	 * Retourne un noeud de Liste
	 * 
	 * @param array $noeuds Liste des éléments
	 * @param array $attributs Liste des attributs de la liste
	 * @param int $ligne Numéro de ligne
	 * @return Object
	**/
	abstract public function Liste($noeuds, $attributs, $ligne);


	/**
	 * Retourne un noeud de Polyglotte
	 * 
	 * @param array $traductions Liste des traductions (couples langue=>texte)
	 * @param int $ligne Numéro de ligne
	 * @return Object
	**/
	abstract public function Polyglotte($traductions, $ligne);
}
