<?php

/**
 * Exception générale
 *
 * Code en partie repris de Twig 1.12.2 (Fabien Potencier)
 */

namespace Spip\Compilo;

/**
 * Exception levée si une erreur est trouvée
 */
class Error extends \Exception {

	protected $lineno;
	protected $filename;
	protected $rawMessage;
	protected $previous;

	public function __construct($message, $lineno = -1, $filename = null, Exception $previous = null)
	{
		parent::__construct('', 0, $previous);

		$this->lineno = $lineno;
		$this->filename = $filename;
		$this->rawMessage = $message;

		$this->updateRepr();
	}


	/**
	 * Met à jour le message avec la ligne et le fichier
	 *
	 * @param 
	 * @return 
	**/
	protected function updateRepr()
	{
		$this->message = $this->rawMessage;

		$dot = false;
		if ('.' === substr($this->message, -1)) {
			$this->message = substr($this->message, 0, -1);
			$dot = true;
		}

		if ($this->filename) {
			if (is_string($this->filename) || (is_object($this->filename) && method_exists($this->filename, '__toString'))) {
				$filename = sprintf('"%s"', $this->filename);
			} else {
				$filename = json_encode($this->filename);
			}
			$this->message .= sprintf(' in %s', $filename);
		}

		if ($this->lineno && $this->lineno >= 0) {
			$this->message .= sprintf(' at line %d', $this->lineno);
		}

		if ($dot) {
			$this->message .= '.';
		}
	}


	/**
	 * Gets the raw message.
	 *
	 * @return string The raw message
	 */
	public function getRawMessage()
	{
		return $this->rawMessage;
	}

	/**
	 * Gets the filename where the error occurred.
	 *
	 * @return string The filename
	 */
	public function getTemplateFile()
	{
		return $this->filename;
	}

	/**
	 * Sets the filename where the error occurred.
	 *
	 * @param string $filename The filename
	 */
	public function setTemplateFile($filename)
	{
		$this->filename = $filename;

		$this->updateRepr();
	}

	/**
	 * Gets the template line where the error occurred.
	 *
	 * @return integer The template line
	 */
	public function getTemplateLine()
	{
		return $this->lineno;
	}

	/**
	 * Sets the template line where the error occurred.
	 *
	 * @param integer $lineno The template line
	 */
	public function setTemplateLine($lineno)
	{
		$this->lineno = $lineno;

		$this->updateRepr();
	}

/*
	public function guess()
	{
		$this->guessTemplateInfo();
		$this->updateRepr();
	}*/
}
