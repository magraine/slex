<?php

/**
 * L'AST est un arbre sémantique des données issues du parseur.
**/

namespace Spip\Compilo;


/**
 * Ast
 *
 * On en fait une classe pour avoir quelques methodes utiles,
 * mais aussi un iterateur de tableau pour le faire comporter
 * comme si on avait un array(), comme le format d'origine de SPIP < 3.
**/
class Ast extends Node
{

	/** Liste des boucles */
	protected $boucles;

	/**
	 * Constructeur
	**/
	public function __construct(array $nodes = array(), array $attributes = array(), $lineno = 0, $tag = null) {
		parent::__construct($nodes, $attributes, $lineno, $tag);
		$this->boucles = array();
	}

	/**
	 * Remplacer les noeuds
	 */
	public function setNodes(Node $nodes) {
		$this->nodes = $nodes->nodes;
	}

	/**
	 * Retourne la liste des boucles classées par nom
	 * @return array()
	**/
	public function getBoucles() {
		return $this->boucles;
	}

}
