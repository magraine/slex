<?php

/**
 * Un noeud est un ensemble d'éléments de l'AST qu'on peut itérer
 *
 * @note
 *    Ce code est grandement copié de la classe Twig_Node dans Twig 1.12.2
 *    Grands remerciements donc à Fabien Potencier et au projet Twig
**/

namespace Spip\Compilo;


/**
 * Noeud de l'Ast
 */
class Node implements \Countable, \IteratorAggregate, \ArrayAccess
{
	/** les noeuds */
	protected $nodes;
	/** les attributs */
	protected $attributes;
	/** le numéro de ligne */
	protected $lineno;
	/** nom de tag du noeud (pour export xml) */
	protected $tag;

	/**
	 * Constructeur.
	 *
	 * Les noeuds peuvent être automatiquement obtenus par propriété ($this->node).
	 * Les attributs, automatiquement comme élément de tableau ($this['name']).
	 *
	 * @param array   $nodes      Liste nommée de noeuds
	 * @param array   $attributes Tableau d'attributs (qui ne sont pas des noeuds)
	 * @param integer $lineno     Numéro de ligne
	 * @param string  $tag        Tag associé au noeud
	 */
	public function __construct(array $nodes = array(), array $attributes = array(), $lineno = 0, $tag = null)
	{
		$this->nodes = $nodes;
		$this->attributes = $attributes;
		$this->lineno = $lineno;
		$this->tag = $tag;
	}

	/**
	 * Export en texte
	 *
	 * @return string
	**/
	public function __toString()
	{
		$attributes = array();
		foreach ($this->attributes as $name => $value) {
			$attributes[] = sprintf('%s: %s', $name, str_replace("\n", '', var_export($value, true)));
		}

		$repr = array(get_class($this).'('.implode(', ', $attributes));

		if (count($this->nodes)) {
			foreach ($this->nodes as $name => $node) {
				$len = strlen($name) + 4;
				$noderepr = array();
				foreach (explode("\n", (string) $node) as $line) {
					$noderepr[] = str_repeat(' ', $len).$line;
				}

				$repr[] = sprintf('  %s: %s', $name, ltrim(implode("\n", $noderepr)));
			}

			$repr[] = ')';
		} else {
			$repr[0] .= ')';
		}

		return implode("\n", $repr);
	}

	/**
	 * Exporter en XML
	 *
	 * @param bool $asDom
	 *     Retourne ou non un DOMDocument, sinon une chaine XML
	 * @return DOMDocument|string
	 *     Texte XML ou objet DOMDocument
	**/
	public function toXml($asDom = false)
	{
		$dom = new \DOMDocument('1.0', 'UTF-8');
		$dom->formatOutput = true;
		$dom->appendChild($xml = $dom->createElement('spip'));

		$xml->appendChild($node = $dom->createElement('node'));
		$node->setAttribute('class', get_class($this));

		foreach ($this->attributes as $name => $value) {
			$node->appendChild($attribute = $dom->createElement('attribute'));
			$attribute->setAttribute('name', $name);
			$attribute->appendChild($dom->createTextNode($value));
		}

		foreach ($this->nodes as $name => $n) {
			if (null === $n) {
				continue;
			}
			// si le contenu du noeud est un simple texte
			// on en fait un tag
			// mais en fait, ça devrait être un attribut dans le cas de notre objet
			// Texte['texte'] et non Texte->texte
			// mais on tache de reproduire le plus fidèlement l'ast attendu par SPIP
			if (!is_object($n)) {
				$node->appendChild($child = $dom->createElement($name, $n));
			} else {
				$child = $n->toXml(true)->getElementsByTagName('node')->item(0);
				$child = $dom->importNode($child, true);
				$child->setAttribute('name', $name);

				$node->appendChild($child);
			}
		}

		return $asDom ? $dom : $dom->saveXml();
	}

	/**
	 * Compile le noeud
	 *
	 * Inutilisé
	 * 
	 * @todo
	 *     Passer le compilateur dessus ?
	 *     Dans une version lointaine pour quelqu'un qui a du courage :)
	 * 
	 * @param Compileur $compileur
	 *     Le compileur à utiliser
	**/
	public function compile(Compiler $compiler) {
		foreach ($this->nodes as $node) {
			$node->compile($compiler);
		}
	}

	/**
	 * Obtient le numéro de ligne
	 *
	 * @return int Numéro de ligne
	**/
	public function getLine() {
		return $this->lineno;
	}

	/**
	 * Obtient le nom du noeud
	 *
	 * @return string Nom du noeud
	**/
	public function getNodeTag() {
		return $this->tag;
	}

	/**
	 * Test si un attribut est défini
	 *
	 * @param string Nom d'attribut
	 * @return bool true si l'attribut est défini, false sinon
	 */
	public function hasAttribute($name)
	{
		return array_key_exists($name, $this->attributes);
	}

	/**
	 * Obtient une valeur d'attribut
	 *
	 * @param string Le nom de l'attribut
	 * @return mixed La valeur de l'attribut
	 */
	public function getAttribute($name) {
		if (!array_key_exists($name, $this->attributes)) {
			throw new \LogicException(sprintf('Attribute "%s" does not exist for Node "%s".', $name, get_class($this)));
		}

		return $this->attributes[$name];
	}

	/**
	 * Définit un attribut
	 *
	 * @param string Nom de l'attribut
	 * @param mixed  Valeur de l'attribut
	 */
	public function setAttribute($name, $value) {
		$this->attributes[$name] = $value;
	}

	/**
	 * Enlève un attribut.
	 *
	 * @param string Nom de l'attribut
	 */
	public function removeAttribute($name) {
		unset($this->attributes[$name]);
	}

	/**
	 * Test si un noeud existe avec le nom donné
	 *
	 * @param string Nom du noeud
	 * @return bool true si le noeud existe
	 */
	public function hasNode($name) {
		return array_key_exists($name, $this->nodes);
	}

	/**
	 * Retourne un noeud par son nom
	 *
	 * @param string Nom du noeud
	 * @return Node Le noeud
	 */
	public function getNode($name) {
		if (!array_key_exists($name, $this->nodes)) {
			throw new \LogicException(sprintf('Node "%s" does not exist for Node "%s".', $name, get_class($this)));
		}

		return $this->nodes[$name];
	}

	/**
	 * Définit un noeud
	 *
	 * @param string Nom du noeud
	 * @return Node $node Le noeud
	 */
	public function setNode($name, $node = null)
	{
		$this->nodes[$name] = $node;
	}

	/**
	 * Enlève un noeud par son nom.
	 *
	 * @param string Nom du noeud
	 */
	public function removeNode($name) {
		unset($this->nodes[$name]);
	}

	/**
	 * Compte le nombre de noeuds
	 *
	 * @return int Nombre de noeuds
	**/
	public function count() {
		return count($this->nodes);
	}

	/**
	 * Retourne l'itérateur sur les noeuds
	 *
	 * @return \ArrayIterator 
	**/
	public function getIterator() {
		return new \ArrayIterator($this->nodes);
	}


	/**
	 * Attribution d'une valeur
	 * 
	 * Ast[] = 'valeur';
	 *
	 * @param null|int|string $offset
	 * @param null|mixed $value
	**/
	public function offsetSet($offset, $value) {
		if (is_null($offset)) {
			$this->nodes[] = $value;
		} else {
			$this->nodes[$offset] = $value;
		}
	}

	/**
	 * Test d'existance d'une clé 
	 * @param int|string $offset
	**/
	public function offsetExists($offset) {
		return isset($this->nodes[$offset]);
	}

	/**
	 * Effacement d'une clé
	 * @param int|string $offset
	**/
	public function offsetUnset($offset) {
		unset($this->nodes[$offset]);
	}

	/**
	 * Obtention d'une clé
	 * @param int|string $offset
	 * @return mixed Valeur de la clé
	**/
	public function offsetGet($offset) {
		return isset($this->nodes[$offset]) ? $this->nodes[$offset] : null;
	}

	/**
	 * Get sur les noms des nodes
	 *
	 * Ceci afin d'assurer la compat avec l'ast de SPIP
	 */
	 public function __get($offset) {
		return isset($this->nodes[$offset]) ? $this->nodes[$offset] : null;
	}

	/**
	 * Set sur les noms des nodes
	 *
	 * Ceci afin d'assurer la compat avec l'ast de SPIP
	 */
	 public function __set($offset, $value) {
		if (is_null($offset)) {
			$this->nodes[] = $value;
		} else {
			$this->nodes[$offset] = $value;
		}
	 }
}
