<?php

/**
 * Exception sur une erreur de compilation
 */
namespace Spip\Compilo\Error;
use Spip\Compilo\Error;

/**
 * Exception levée si une erreur est trouvée dans le lexeur ou parseur
 */
class Syntax extends Error{}
