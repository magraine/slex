<?php

/**
 * Noeud Texte
**/

namespace Spip\Compilo\Node;
use Spip\Compilo\Node;
use Spip\Compilo\Compiler; # inutilisé


/**
 * Represente un noeud de polyglotte
 */
class Polyglotte extends Node
{
	/**
	 * Constructeur
	 *
	 * @param string $texte
	 * @param int $lineno
	**/
	public function __construct($traductions, $lineno)
	{
		// type et texte devraient être des attributs et non des noeuds
		// mais on essaie de coller à l'ast attendu par SPIP
		parent::__construct(array(
			'type'  => 'polyglotte',
			'traductions' => $traductions,
			'ligne' => $lineno
		), array(), $lineno);
	}

    /**
     * Compile le noeud
     * @param Compiler
     */
    public function compile(Compiler $compiler) {}
}
