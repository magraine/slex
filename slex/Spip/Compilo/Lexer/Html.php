<?php

/**
 * Lexeur des squelettes SPIP standards
**/

namespace Spip\Compilo\Lexer;

use Spip\Compilo\Token;
use Spip\Compilo\TokenStream;
use Spip\Compilo\Error;

/**
 * Lexeur Spip/Html
 *
 * Transforme un code de squelette en tokens
**/
class Html extends Base {

	/**
	 * Constructeur
	 *
	 * Initialise le lexeur.
	 *
	 * @param array $options
	 *     Options
	**/
	public function __construct($options = array()) {

		// options de description des tags
		$this->tags = array_merge(array(
			'echappement'         => array('\\', '[](){}<>#'), # échappant, echappés…
			'boucle_ouvrir'        => array('<BOUCLE',  '>'),
			'boucle_fermer'        => array('</BOUCLE', '>'),
			'boucle_altern_ouvrir' => array('<B',   '>'), # peut être confondu avec <B>bold</B>
			'boucle_altern_fermer' => array('</B',  '>'),
			'boucle_sinon'         => array('<//B', '>'),
			'idiome'               => array('<:',   ':>'),
			'polyglotte'           => array('<multi>', '</multi>'),
			# ces éléments peuvent être soit du SPIP soit du texte… le parseur déterminera
			'balise'               => array('#'),
			'balise_altern'        => array('[', ']'),
			'balise_altern_si'     => array('(', ')'),
			'argument'             => array('{', '}'),
			'filtre'               => array('|'),
			
		), isset($options['tags']) ? $options['tags'] : array());
		unset($options['tags']);

		// autres options
		$this->options = array_merge(array(), $options);

		// liste des regexps utilisées
		$this->regexes = array(
			'lex_decoupes_debuts' => '/('
				# echappements
				. preg_quote($this->tags['echappement'][0]) . '['  . preg_quote($this->tags['echappement'][1]) . ']'
				# polyglotte
				. '|' . preg_quote($this->tags['polyglotte'][0], '/')
				# idiome
				. '|' . preg_quote($this->tags['idiome'][0], '/')
				# la fin…
				. ')/s',
			'lex_langue_polyglotte' => '/([[{])([a-z_]+)([]}])/si',
			'lex_fin_polyglotte'    => '/(?:'.preg_quote($this->tags['polyglotte'][1], '/').')/s',
			'lex_fin_idiome'        => '/(?:'.preg_quote($this->tags['idiome'][1], '/').')/s', 
		);

	}

	/**
	 * Découpe un code de squelette SPIP en lexèmes
	 *
	 * @param string $code
	 *     Code source
	 * @param string|null $filename
	 *     Chemin du fichier (si le code provient d'un fichier)
	 * @return TokenStream
	 *     Instance TokenStream, itérateur sur les tokens.
	**/
	public function tokenize($code, $filename = null) {
		// comme Twig, être sur d'utiliser str_ natifs de PHP
		if (function_exists('mb_internal_encoding') && ((int) ini_get('mbstring.func_overload')) & 2) {
			$mbEncoding = mb_internal_encoding();
			mb_internal_encoding('ASCII');
		}

		// initialiser (et homogénéiser les retours chariots)
		$this->code = str_replace(array("\r\n", "\r"), "\n", $code);
		$this->filename = $filename;
		$this->cursor = 0;
		$this->lineno = 1;
		$this->end = strlen($this->code);
		$this->tokens = array();
		$this->state = self::STATE_DATA;
		$this->position = -1;

		// trouve les tokens ouvrants
		preg_match_all($this->regexes['lex_decoupes_debuts'], $this->code, $matches, PREG_OFFSET_CAPTURE);

		// pour chaque début de position trouvée, on détermine son type
		// pour faciliter l'analyse ultérieure
		if (count($matches) and $i = count($matches[0])) {
			$ligne = 1;
			for ($n = 0; $n < $i; $n ++) {
				switch ($matches[1][$n][0]) {
					case $this->tags['polyglotte'][0]:
						$matches[0][$n][2] = self::DEBUT_POLYGLOTTE;
						break;
					case $this->tags['idiome'][0]:
						$matches[0][$n][2] = self::DEBUT_IDIOME;
						break;
					// sinon c'est un échappement, mais on verifiera par prudence.
					default:
						// verifier que c'est un echappement par prudence
						if (0 === strpos($echap = $matches[1][$n][0], $this->tags['echappement'][0])
						  AND $char = substr($echap, strlen($this->tags['echappement'][0]))
						  AND strlen($char) == 1
						  AND strpbrk($char, $this->tags['echappement'][1]))
						{
							$matches[0][$n][2] = self::DEBUT_ECHAPPEMEMT;
							break;
						}

						throw new Error\Syntax('Début de position indéterminée ' . $matches[0][$n][2], $this->lineno, $this->filename);
						break;
				}
			}
		}

		$this->positions = $matches;

		while ($this->cursor < $this->end) {
			// distribuer l'action à réaliser en fonction de l'état courant
			switch ($this->state) {
				case self::STATE_DATA:
					$this->lexData();
					break;
				case self::STATE_POLYGLOTTE:
					$this->lexPolyglotte();
					break;
				case self::STATE_IDIOME:
					$this->lexIdiome();
					break;
			}
		}

		$this->pushToken(Token::ST_EOF);


		// Remettre l'encodage
		if (isset($mbEncoding)) {
			mb_internal_encoding($mbEncoding);
		}

		return new TokenStream($this->tokens, $this->filename);
	}


	/**
	 * Crée les lexèmes pour une donnée indéterminée (du texte certainement)
	**/
	protected function lexData()
	{
		// s'il ne reste plus d'élément spécifiques après notre position, ce n'est que du texte.
		if (false !== ($texte = $this->dernierePosition())) {
			$this->pushToken(Token::ST_TEXTE, $texte);
			$this->cursor = $this->end;
			return;
		}

		// Trouver le premier token après (ou égal à) la position du curseur
		if (false === ($position = $this->avancerProchainePosition())) {
			return; # reviendra dans lexData() et retournera le texte.
		}

		// Envoyer ce qu'on a comme texte entre le curseur et le début du nouvel élément
		$text = $textContent = substr($this->code, $this->cursor, $position[1] - $this->cursor);
		$this->pushToken(Token::ST_TEXTE, $text);

		// on avance le curseur, mais contrairement à Twig,
		//_on avance juste AVANT l'élément trouvé, pas juste après
		$this->moveCursor($textContent); # $textContent.$position[0]

		// En fonction de l'élément capturé sur lequel on est arrivé
		switch ($this->positions[0][$this->position][2]) {
			case self::DEBUT_ECHAPPEMEMT:
				$this->lexEchappement();
				break;

			case self::DEBUT_POLYGLOTTE:
				$this->pushToken(Token::ST_POLYGLOTTE_DEBUT, $this->tags['polyglotte'][0]);
				$this->pushState(self::STATE_POLYGLOTTE);
				$this->moveCursor($this->tags['polyglotte'][0]);
				break;

			case self::DEBUT_IDIOME:
				$this->pushToken(Token::ST_IDIOME_DEBUT, $this->tags['idiome'][0]);
				$this->pushState(self::STATE_IDIOME);
				$this->moveCursor($this->tags['idiome'][0]);
				break;

			// sinon bah… on fait quoi ?
			default:
				throw new Error\Syntax("Position " . $this->position . " de type indéterminée sur : "
					. $this->positions[0][$this->position][0], $this->lineno, $this->filename);
				break;
		}
	}



	/**
	 * Crée les lexèmes d'un polyglotte
	 *
	 * On entre ici avec l'état polyglotte normallement.
	**/
	protected function lexPolyglotte()
	{
		// s'il ne reste plus d'éléments spécifiques après notre position, ce n'est que du texte.
		if (false !== ($texte = $this->dernierePosition())) {

			// on doit donc forcément avoir une fermeture de polyglotte.
			if (!preg_match($this->regexes['lex_fin_polyglotte'], $texte, $match, PREG_OFFSET_CAPTURE)) {
				throw new Error\Syntax('Fin de polyglotte introuvable', $this->lineno, $this->filename);
			}

		} else {

			// Trouver le premier token après (ou égal à) la position du curseur
			if (false === ($position = $this->avancerProchainePosition())) {
				throw new Error\Syntax('Fin de polyglotte introuvable', $this->lineno, $this->filename);
			}

			// Texte entre le curseur et l'élément qui suit
			$texte = substr($this->code, $this->cursor, $position[1] - $this->cursor);
		}


		// s'il y a une fin de polyglotte dans le texte, il faut analyser
		// ce qui est avant, puis fermer le polyglotte et quitter l'état polyglotte
		$fin = false;
		if ($texte and preg_match($this->regexes['lex_fin_polyglotte'], $texte, $match, PREG_OFFSET_CAPTURE)) {
			$fin = true;
			$texte = substr($texte, 0, $match[0][1]);
		}


		// si on a du texte, on regarde ce qui est relatifs aux déclarations
		// de langue [en] du reste...
		if ($texte) {
			$langue = '';

			// pour chaque déclaration de changement de langue
			if (preg_match_all($this->regexes['lex_langue_polyglotte'], $texte, $regs, PREG_OFFSET_CAPTURE)) {
				$pos = 0;
				foreach ($regs[0] as $i => $langue) { // $langue : array('[en]', 8)
					// texte avant la déclaration
					$t = substr($texte, $pos, $langue[1] - $pos);
					if (strlen($t)) {
						$this->pushToken(Token::ST_TEXTE, $t);
					}
					// déclaration de langue
					$this->pushToken(Token::ST_POLYGLOTTE_LANGUE, $regs[2][$i][0], $regs[1][$i][0], $regs[3][$i][0]);
					$pos = $langue[1] + strlen($langue[0]);
				}
				// texte après la dernière déclaration
				$t = substr($texte, $pos);
				if (strlen($t)) {
					$this->pushToken(Token::ST_TEXTE, $t);
				}
			} else {
				// sinon c'est que le texte n'est pas découpé en plusieurs langues.
				$this->pushToken(Token::ST_TEXTE, $texte);
			}

			$this->moveCursor($texte);
		}

		// fin détectée, on l'ajoute
		if ($fin) {
			$this->pushToken(Token::ST_POLYGLOTTE_FIN,   $this->tags['polyglotte'][1]);
			$this->moveCursor($this->tags['polyglotte'][1]);
			$this->popState();
			return;
		}

		// si on n'était pas à la toute dernière position, et qu'on a toujours
		// pas trouvé la fin du polyglotte, il faut analyser la suite,
		// en fonction de l'élément capturé sur lequel on est arrivé
		switch ($this->positions[0][$this->position][2]) {
			case self::DEBUT_ECHAPPEMEMT:
				$this->lexEchappement();
				break;

			// sinon bah… sinon c'est pas normal
			default:
				throw new Error\Syntax(
					  "Lexer HTML : Un polyglotte ne peut pas contenir autre chose que "
					. "du texte, ou échappements. "
					. self::positionCodeToString($this->positions[0][$this->position][2], true, $this->lineno)
					. " trouvé",
					$this->lineno, $this->filename);
				break;
		}
	}





	/**
	 * Crée les lexèmes d'un idiome
	 *
	 * On entre ici avec l'état idiome normalement.
	**/
	protected function lexIdiome()
	{
		// s'il ne reste plus d'éléments spécifiques après notre position, ce n'est que du texte.
		if (false !== ($texte = $this->dernierePosition())) {

			// on doit donc forcément avoir une fermeture d'idiome.
			if (!preg_match($this->regexes['lex_fin_idiome'], $texte, $match, PREG_OFFSET_CAPTURE)) {
				throw new Error\Syntax('Fin d\'idiome introuvable', $this->lineno, $this->filename);
			}

		} else {

			// Trouver le premier token après (ou égal à) la position du curseur
			if (false === ($position = $this->avancerProchainePosition())) {
				throw new Error\Syntax('Fin d\'idiome introuvable', $this->lineno, $this->filename);
			}

			// Texte entre le curseur et l'élément qui suit
			$texte = substr($this->code, $this->cursor, $position[1] - $this->cursor);
		}

		// s'il y a une fin d'idiome dans le texte, il faut analyser
		// ce qui est avant, puis fermer l'idiome et quitter l'état idiome
		$fin = false;
		if ($texte and preg_match($this->regexes['lex_fin_idiome'], $texte, $match, PREG_OFFSET_CAPTURE)) {
			$fin = true;
			$texte = substr($texte, 0, $match[0][1]);
		}


		// si on a du texte, on cherche s'il existe un module de langue spécifique
		// pour l'instant, juste cle:texte
		if ($texte) {
			$module = '';
			$cle    = '';
			if (false === $pos = strpos($texte, ':')) {
				$this->pushToken(Token::ST_IDIOME_CLE, $texte);
			} else {
				$module = substr($texte, 0, $pos);
				$cle = substr($texte, $pos+1);
				$this->pushToken(Token::ST_IDIOME_MODULE, $module, '', ':');
				$this->pushToken(Token::ST_IDIOME_CLE, $cle);
			}

			$this->moveCursor($texte);
		}

		// fin détectée, on l'ajoute
		if ($fin) {
			$this->pushToken(Token::ST_IDIOME_FIN,   $this->tags['idiome'][1]);
			$this->moveCursor($this->tags['idiome'][1]);
			$this->popState();
			return;
		}

		// si on n'était pas à la toute dernière position, et qu'on n'a toujours
		// pas trouvé la fin de l'idiome, il faut analyser la suite,
		// en fonction de l'élément capturé sur lequel on est arrivé
		switch ($this->positions[0][$this->position][2]) {
			case self::DEBUT_ECHAPPEMEMT:
				$this->lexEchappement();
				break;

			// sinon bah… sinon c'est pas normal
			default:
				throw new Error\Syntax(
					  "Lexer HTML : Un idiome ne peut pas contenir autre chose que "
					. "du texte ou échappements. "
					. self::positionCodeToString($this->positions[0][$this->position][2], true, $this->lineno)
					. " trouvé",
					$this->lineno, $this->filename);
				break;
		}
	}


	/**
	 * Crée les lexèmes d'un échappement de caractère
	**/
	protected function lexEchappement()
	{
		$echap = $this->positions[1][$this->position][0];
		$char = substr($echap, strlen($this->tags['echappement'][0]));
		$this->pushToken(Token::ST_ECHAPPEMENT, $char, $this->tags['echappement'][0]);
		$this->moveCursor($echap);
	}

}
