<?php

/**
 * Base commune entre les lexeurs de Slex
**/

namespace Spip\Compilo\Lexer;

use Spip\Compilo\Lexer;
use Spip\Compilo\Token;
use Spip\Compilo\TokenStream;


/**
 * Lexeur
 *
 * Transforme un code de squelette en tokens
**/
abstract class Base extends Lexer {

	/** Liste des tokens */
	protected $tokens;
	/** Code source */
	protected $code;
	/** Position du curseur */
	protected $cursor;
	/** N° de ligne */
	protected $lineno;
	/** Position du dernier caractère du code source */
	protected $end;
	/** Regexes */
	protected $regexes;
	/** Options */
	protected $options;
	/** Options d'analyse des tags */
	protected $tags;
	/** Découpage initial par regexp : 1 élément par changement */
	protected $positions;
	/** Position actuelle de l'analyse dans les positions du découpage */
	protected $position;


	/** État indéterminé (texte en dehors d'un contexte spip) */
	const STATE_DATA            = 0;
	/** État polyglotte (contenu d'un polyglotte) */
	const STATE_POLYGLOTTE      = 1;
	/** État Idiome (contenu d'un idiome) */
	const STATE_IDIOME          = 2;


	/** Débuts de position echappement */
	const DEBUT_ECHAPPEMEMT     = 1;
	/** Débuts de position commentaire */
	const DEBUT_COMMENTAIRE     = 2;
	/** Débuts de position polyglotte */
	const DEBUT_POLYGLOTTE      = 3;
	/** Débuts de position idiome */
	const DEBUT_IDIOME          = 4;


	/**
	 * Constructeur
	 *
	 * Initialise le lexeur.
	 *
	 * @param array $options
	 *     Options
	**/
	public function __construct($options = array()) {
		// options de description des tags
		$this->tags = array();

		// autres options
		$this->options = array();

		// liste des regexps utilisées
		$this->regexes = array();
	}




	/**
	 * Test s'il ne reste plus de position après notre curseur.
	 *
	 * @return bool|string
	 *     - false s'il reste encore des tokens
	 *     - string : texte entre le curseur et la fin du code sinon.
	**/
	protected function dernierePosition()
	{
		// pas de position, forcément oui
		if (!count($this->positions[0])) {
			return substr($this->code, $this->cursor);
		}

		// si notre curseur est avant la position actuelle, ce n'est pas la fin
		if (
				# pas la position initiale -1 s'il y a des position
				   $this->position < 0
				# pas si le curseur est placé avant la position actuelle (qui reste donc à traiter)
				OR $this->cursor <= $this->positions[0][$this->position][1]
				# pas si ce n'est pas la toute dernière position
				OR ($this->position != count($this->positions[0]) - 1)
			)
		{
			return false;
		}

		// tout ce qui suit est du texte.
		return substr($this->code, $this->cursor);

	}

	/**
	 * Retourne la prochaine position après le curseur et avance
	 * le pointeur de position au passage.
	 *
	 * @return bool|array
	 *     - false s'il n'y a aucune position après le curseur, sinon
	 *     - array description de la position 
	**/
	protected function avancerProchainePosition()
	{
		if ($this->position < 0) $this->position++;

		$position = $this->positions[0][$this->position];
		while ($position[1] < $this->cursor) {
			if ($this->position == count($this->positions[0]) - 1) { # c'était le dernier !
				return false;
			}
			$position = $this->positions[0][++$this->position];
		}
		return $position;
	}

	/**
	 * Ajoute un token au flux
	 *
	 * @param int $type Type du token
	 * @param string $value Valeur éventuelle
	 * @param string $avant Élément de syntaxe avant éventuelle
	 * @param string $apres Élément de syntaxe apres éventuelle
	**/
	protected function pushToken($type, $value = '', $avant = null, $apres = null) {
		// Pas de texte vide
		if (Token::ST_TEXTE === $type
		&& '' === $value && !$avant && !$apres) {
			return;
		}

		$this->tokens[] = new Token($type, $value, $this->lineno, $avant, $apres);
	}


	/**
	 * Déplacer le curseur de la longueur du texte transmis
	 *
	 * Actualise aussi le numéro de ligne en cours.
	 *
	 * @param string $text
	**/
	protected function moveCursor($text)
	{
		$this->cursor += strlen($text);
		$this->lineno += substr_count($text, "\n");
	}

	/**
	 * Empile un nouvel état 
	 *
	 * @param int $state État
	**/
	protected function pushState($state)
	{
		$this->states[] = $this->state;
		$this->state = $state;
	}

	/**
	 * Démpile un état (et remet donc l'état précédent)
	**/
	protected function popState()
	{
		if (0 === count($this->states)) {
			throw new \Exception('Cannot pop state without a previous state');
		}

		$this->state = array_pop($this->states);
	}

	/**
	 * Retourne le type de début en fonction de son code
	 *
	 * @param int    $type  Code de début de position
	 * @param bool   $short Représentation courte ?
	 * @param int    $line  Numéro de ligne
	 *
	 * @return string Code sous forme présentable
	**/
	public static function positionCodeToString($type, $short = false, $line = -1) {
		switch ($type) {
			case self::DEBUT_ECHAPPEMEMT:
				$name = 'DEBUT_ECHAPPEMEMT';
				break;
			case self::DEBUT_COMMENTAIRE:
				$name = 'DEBUT_COMMENTAIRE';
				break;
			case self::DEBUT_POLYGLOTTE:
				$name = 'DEBUT_POLYGLOTTE';
				break;
			case self::DEBUT_IDIOME:
				$name = 'DEBUT_IDIOME';
				break;

			default:
				throw new \LogicException(sprintf('Position start of type "%s" does not exist.', $type));
		}

		return $short ? $name : 'Lexer::'.$name;
	}
}
