<?php

/**
 * Le lexeur (lexer en anglais) analyse un code source et le transforme
 * en entités lexicales (aussi nommées lexèmes, ou tokens en anglais).
 *
 * Un token (lexème) est un élément unique identifiant une partie
 * du code source comme étant un élément structurant, faisant partie
 * de la syntaxe du langage (de squelettes SPIP).
 *
 * Ainsi `<BOUCLE` dans un code source sera probablement un token
 * d'ouverture de boucle. `<:` probablement un d'idiome. etc.
 *
 * L'analyseur lexical identifie les lexèmes et les classe.
 * S'il détecte une entité lexicale invalide, il rapporte une erreur.
 * Généralement, les combinaisons d'entités lexicales sont laissées au
 * soin de l'analyseur (parser en anglais), comme la bonne cohérence
 * des parenthèses.
 * 
 * @note
 * Un compilateur effectue les opérations suivantes :
 * - analyse lexicale,
 * - pré-traitement (préprocesseur),
 * - décomposition analytique (parsing),
 * - analyse sémantique,
 * - génération de code
 * - et optimisation de code.
**/

namespace Spip\Compilo;


/**
 * Lexeur
 *
 * Classe abstraite que doivent implémenter les Lexeurs
**/
abstract class Lexer {

	/**
	 * Constructeur
	 *
	 * @param array $options Tableau d'options pour le lexeur
	**/
	abstract public function __construct($options = array());

	/**
	 * Découpe un code en lexèmes et retourne un flux de lexèmes
	 *
	 * @param string $code
	 *     Code source
	 * @param string|null $filename
	 *     Chemin du fichier (si le code provient d'un fichier)
	 * @return TokenStream
	 *     Instance TokenStream, itérateur sur les tokens.
	**/
	abstract function tokenize($code, $filename = null);


	/**
	 * Crée un lexer du type demandé et le retourne
	 *
	 * @param string $type
	 * @param array $options
	 * @return Lexer
	**/
	public static function Lexer($type, $options = array()) {
		$l = "Spip\Compilo\Lexer\\$type";
		return new $l($options);
	}
}
