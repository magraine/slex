<?php

/**
 * Expression d'un token
 */

namespace SPIP\Compilo;

/**
 * Représente un token
 */
class Token
{
	/** valeur du token */
	protected $value;
	/** type de token */
	protected $type;
	/** numéro de ligne */
	protected $lineno;

	/**
	 * Valeur avant le token
	 *
	 * Élément de syntaxe, mais inutile pour l'information, sauf pour restituer le code source.
	 */
	protected $avant;

	/**
	 * Valeur après le token
	 *
	 * Élément de syntaxe, mais inutile pour l'information, sauf pour restituer le code source.
	 */
	protected $apres;


	/** SPIP Token Fin de fichier */
	const ST_EOF                  = -1;

	/** SPIP Token Texte */
	const ST_TEXTE                = 1;
	/** SPIP Token Echap */
	const ST_ECHAPPEMENT          = 2;

	/** SPIP Token Commentaire */
	const ST_COMMENTAIRE          = 10; // php : T_COMMENT
	/** SPIP Token Commentaire de doc */
	const ST_COMMENTAIRE_DOC      = 11; // php : T_DOC_COMMENT

	/** SPIP Token Polyglotte ouvrant */
	const ST_POLYGLOTTE_DEBUT     = 20;
	/** SPIP Token Polyglotte fermant */
	const ST_POLYGLOTTE_FIN       = 21;
	/** SPIP Token Polyglotte définition de langue */
	const ST_POLYGLOTTE_LANGUE    = 22;

	/** SPIP Token Idiome ouvrant */
	const ST_IDIOME_DEBUT         = 30;
	/** SPIP Token Idiome ouvrant */
	const ST_IDIOME_FIN           = 31;
	/** SPIP Token Idiome module */
	const ST_IDIOME_MODULE        = 32;
	/** SPIP Token Idiome cle */
	const ST_IDIOME_CLE           = 33;


	/**
	 * Constructeur
	 *
	 * @param int    $type   Type de token
	 * @param string $value  Valeur du token
	 * @param int    $lineno Numéro de ligne dans la source
	 */
	public function __construct($type, $value, $lineno, $avant = null, $apres = null)
	{
		$this->type   = $type;
		$this->value  = $value;
		$this->lineno = $lineno;
		$this->avant  = $avant;
		$this->apres  = $apres;
	}

	/**
	 * Retourne une représentation textuelle du token
	 *
	 * @return string 
	 */
	public function __toString()
	{
		return sprintf('%s(%s)', self::typeToString($this->type, true, $this->lineno), $this->value);
	}

	/**
	 * Test que le token a bien le type et enventuellement la valeur indiquée
	 * 
	 * @param integer           $type   The type de token
	 * @param array|string|null $values Valeur éventuelle (ou liste de valeurs possibles)
	 * @return bool
	 */
	public function test($type, $values = null)
	{
		return ($this->type === $type) && (
			null === $values ||
			(is_array($values) && in_array($this->value, $values)) ||
			$this->value == $values
		);
	}

	/**
	 * Retourne la ligne dans le code source du token
	 * @return int Ligne dans la source.
	 */
	public function getLine()
	{
		return $this->lineno;
	}

	/**
	 * Retourne le type de token
	 * @return int Type de token
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * Retourne la valeur du token
	 * @return string Valeur du token
	 */
	public function getValue()
	{
		return $this->value;
	}

	/**
	 * Retourne un élément de syntaxe avant
	 * @return string Syntaxe avant
	 */
	public function getAvant()
	{
		return $this->avant;
	}

	/**
	 * Retourne un élément de syntaxe apres
	 * @return string Syntaxe avant
	 */
	public function getApres()
	{
		return $this->apres;
	}

	/**
	 * Retourne le code de représentation interne d'un type de token
	 *
	 * @param int     $type  Type de token
	 * @param bool    $short Représentation courte ?
	 * @param int     $line  Numéro de ligne
	 *
	 * @return string Texte représentant le type de token
	 */
	public static function typeToString($type, $short = false, $line = -1)
	{
		switch ($type) {
			case self::ST_EOF:
				$name = 'ST_EOF';
				break;
			case self::ST_TEXTE:
				$name = 'ST_TEXTE';
				break;
			case self::ST_ECHAPPEMENT:
				$name = 'ST_ECHAPPEMENT';
				break;
			case self::ST_COMMENTAIRE:
				$name = 'ST_COMMENTAIRE';
				break;
			case self::ST_COMMENTAIRE_DOC:
				$name = 'ST_COMMENTAIRE_DOC';
				break;
			case self::ST_POLYGLOTTE_DEBUT:
				$name = 'ST_POLYGLOTTE_DEBUT';
				break;
			case self::ST_POLYGLOTTE_FIN:
				$name = 'ST_POLYGLOTTE_FIN';
				break;
			case self::ST_POLYGLOTTE_LANGUE:
				$name = 'ST_POLYGLOTTE_LANGUE';
				break;
			case self::ST_IDIOME_DEBUT:
				$name = 'ST_IDIOME_DEBUT';
				break;
			case self::ST_IDIOME_FIN:
				$name = 'ST_IDIOME_FIN';
				break;
			case self::ST_IDIOME_MODULE:
				$name = 'ST_IDIOME_MODULE';
				break;
			case self::ST_IDIOME_CLE:
				$name = 'ST_IDIOME_CLE';
				break;

			default:
				throw new \LogicException(sprintf('Token of type "%s" does not exist.', $type));
		}

		return $short ? $name : 'Token::'.$name;
	}

	/**
	 * Retourne une représentation en chaîne de langue du type de token
	 *
	 * @param int $type Type de token
	 * @param int $line Numéro de ligne dans la source
	 *
	 * @return string Texte représentant le token
	 */
	public static function typeToEnglish($type, $line = -1)
	{
		switch ($type) {
			case self::ST_EOF:
				return 'end of template';
			case self::ST_TEXTE:
				return 'text';
			case self::ST_ECHAPPEMENT:
				return 'echap';
			case self::ST_COMMENTAIRE:
				return 'comment';
			case self::ST_COMMENTAIRE_DOC:
				return 'doc comment';
			case self::ST_POLYGLOTTE_DEBUT:
				return 'début de polyglotte';
			case self::ST_POLYGLOTTE_FIN:
				return 'fin de polyglotte';
			case self::ST_POLYGLOTTE_LANGUE:
				return 'langue de polyglotte';
			case self::ST_IDIOME_DEBUT:
				return 'début d\'idiome';
			case self::ST_IDIOME_FIN:
				return 'fin d\'idiome';
			case self::ST_IDIOME_MODULE:
				return 'module d\'idiome';
			case self::ST_IDIOME_CLE:
				return 'cle de langue d\'idiome';
			default:
				throw new LogicException(sprintf('Token of type "%s" does not exist.', $type));
		}
	}
}
