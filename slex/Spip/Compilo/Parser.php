<?php

/**
 * Le parseur analyse un flux de lexème pour obtenir un arbre
 * sémantique des données.
 *
 * @note
 *    Ce code est pour partie copié de la classe Twig_Parser dans Twig 1.12.2
 *    Grands remerciements donc à Fabien Potencier et au projet Twig
 * 
 * @note
 * Un compilateur effectue les opérations suivantes :
 * - analyse lexicale,
 * - pré-traitement (préprocesseur),
 * - décomposition analytique (parsing),
 * - analyse sémantique,
 * - génération de code
 * - et optimisation de code.
**/

namespace Spip\Compilo;


/**
 * Parseur
 *
 * Transforme un flux de tokens en Arbre de syntaxe abstraite.
**/
class Parser {

	/** options */
	protected $options;

	/** liste des états passés */
	protected $stack = array();

	/** flux de tokens */
	protected $stream;

	/** parent ? */
	protected $parent;

	/** Instance d'Ast, le type d'arbre que l'on crée */
	protected $ast;

	/**
	 * Constructeur
	 *
	 * Initialise le parseur.
	 *
	 * @param array $options
	 *     Options
	**/
	public function __construct($options = array()) {
		// options
		$this->options = array_merge(array(), $options);
	}

	/**
	 * Parse un flux de lexèmes
	 *
	 * @param TokenStream $stream
	 *     Code source
	 * @param Ast $ast
	 *     Instance d'un Ast, le type d'Ast que l'on souhaite générer
	 * @return Ast
	 *     Instance de l'Ast
	**/
	public function parse(TokenStream $stream, Ast $ast = null) {
		// l'ast traditionnel de SPIP par défaut, si non fourni
		if (is_null($ast)) {
			$ast = new Ast\Original();
		}

		// on met toutes les variables actuelles dans une pile pour en garder puis remettre l'état
		// car on peut revenir là par des sous analyses
		$vars = get_object_vars($this);
		unset($vars['stack'], $vars['options']);
		$this->stack[] = $vars;

		$this->stream = $stream;
		$this->ast    = $ast;
		$this->parent = null;

		try {
			$nodes = $this->subparse();

		} catch (Error\Syntax $e) {
			// Todo: methodes ?
			if (!$e->getTemplateFile()) {
				$e->setTemplateFile($this->getFilename());
			}

			if (!$e->getTemplateLine()) {
				$e->setTemplateLine($this->stream->current()->getLine());
			}

			throw $e;
		}

		// notre ast en préparation
		$ast->setNodes($nodes);

		// restaurer la pile précédente
		foreach (array_pop($this->stack) as $key => $val) {
			$this->$key = $val;
		}
		
		return $ast;
	}

	/**
	 * Parsage des tokens
	 *
	 * @param null|string $test
	 *     Type de test, si indiqué
	 * @param bool $dropNeedle
	 *     ?
	**/
	public function subparse()
	{
		$ast = $this->ast;
		$lineno = $this->getCurrentToken()->getLine();
		$rv = array();
		while (!$this->stream->isEOF()) {
			switch ($this->getCurrentToken()->getType()) {

				case Token::ST_TEXTE:
					$token = $this->stream->current();
					$rv[] = $ast->Texte($token->getValue(), $token->getAvant(), $token->getApres(), $token->getLine());
					$this->stream->next();
					break;

				case Token::ST_COMMENTAIRE:
				case Token::ST_COMMENTAIRE_DOC:
					$this->stream->next(); // on ne fait rien pour eux !
					break;

				case Token::ST_ECHAPPEMENT:
					$token = $this->stream->current();
					$rv[] = $ast->Texte($token->getValue(), null, null, $token->getLine());
					$this->stream->next();
					break;

				case Token::ST_POLYGLOTTE_DEBUT:
					$token = $this->stream->current();
					// la ligne correspond au token d'ouverture
					$ligne = $token->getLine();
					// prochain token qui n'est pas un commentaire
					$this->stream->next();
					$this->stripWhitespaces();
					// polyglotte retourne le premier élément s'il ne trouve pas de langue approprié.
					// on peut le nommer, ce qui évite une erreur sur la génération du xml
					$langue = 'defaut';
					$texte = '';

					// On parcours les prochains tokens jusqu'à trouver un token de fin de polyglotte.
					// Si entre les deux on trouve des tokens inapproprié, il y a une erreur de syntaxe.
					// de même s'il n'y a pas de fin de polyglotte !
					$token = $this->stream->current();

					// conteneur réceptionnant les traductions
					$trads = array();

					while ($token->getType() !== Token::ST_POLYGLOTTE_FIN) {

						if (!in_array($token->getType(), array(
							Token::ST_POLYGLOTTE_LANGUE,
							Token::ST_TEXTE,
							Token::ST_ECHAPPEMENT
						))) {
							throw new Error\Syntax(
								  'Un polyglotte (ligne ' . $ligne . ') ne peut contenir de type '
								. Token::typeToString($token->getType())
								. ". Avez vous bien fermé ce polyglotte ?",
								$token->getLine(), $this->getFilename());
						}

						// soit déclaration de langue, soit du texte.
						if ($token->getType() == Token::ST_POLYGLOTTE_LANGUE) {
							// depilage des textes capturés s'il y a
							if ($texte) {
								if ($texte = trim($texte)) {
									$trads[$langue] = $texte;
								}
								$texte = '';
							}
							$langue = $token->getValue();
						} else {
							// empilage des textes (texte + commentaire + texte => 1 seul texte)
							$texte .= $token->getValue();
						}
						$this->stream->next();
						$this->stripWhitespaces();
						$token = $this->stream->current();
					}

					// depilage des textes capturés s'il y a
					if ($texte and $texte = trim($texte)) {
						$trads[$langue] = $texte;
					}

					$rv[] = $ast->Polyglotte($trads, $ligne);
					$this->stream->next();
					break;


				// gestion pour l'instant sommaire des idiomes
				case Token::ST_IDIOME_DEBUT:
					$token = $this->stream->current();
					// la ligne correspond au token d'ouverture
					$ligne = $token->getLine();
					// prochain token qui n'est pas un commentaire
					$this->stream->next();
					$this->stripWhitespaces();

					// un idiome peut avoir un module et doit avoir une clé de langue
					$module = 'public|spip|ecrire';
					$cle    = '';

					// On parcours les prochains tokens pour trouver un module et une clé de langue
					$token = $this->stream->current();

					// module de langue ?
					if ($token->getType() == Token::ST_IDIOME_MODULE) {
						$module = $token->getValue();
						$this->stream->next();
						$this->stripWhitespaces();
						$token = $this->stream->current();
					}

					// sinon cle de langue
					if ($token->getType() == Token::ST_IDIOME_CLE) {
						$cle = $token->getValue();
						$this->stream->next();
						$this->stripWhitespaces();
						$token = $this->stream->current();
					}

					// sinon problème !
					else {
						throw new Error\Syntax(
							  'Un idiome (ligne ' . $ligne . ') ne peut contenir de type '
							. Token::typeToString($token->getType())
							. ". Avez vous bien fermé cet idiome ?",
							$token->getLine(), $this->getFilename());
					}

					if ($token->getType() !== Token::ST_IDIOME_FIN) {
						throw new Error\Syntax(
							  'Un idiome (ligne ' . $ligne . ') ne peut contenir de type '
							. Token::typeToString($token->getType())
							. ". Avez vous bien fermé cet idiome ?",
							$token->getLine(), $this->getFilename());
					}

					$rv[] = $ast->Idiome($module, $cle, $ligne);
					$this->stream->next();
					break;





				default:
					throw new Error\Syntax('Lexer or parser ended up in unsupported state.', 0, $this->getFilename());
			}
		}

		// Liste concatènera 2 textes consécutifs si c'est possible
		return $ast->Liste($rv, array(), $lineno);
	}

	/**
	 * Avance jusqu'au prochain token qui n'est ni un
	 * espace, ni un commentaire
	**/
	protected function stripWhitespaces() {
		while (in_array($this->stream->current()->getType(), array(
			Token::ST_COMMENTAIRE,
			Token::ST_COMMENTAIRE_DOC,
		))) {
			$this->stream->next();
		}
		#var_dump(Token::typeToString($this->stream->current()->getType()));
	}



	/**
	 * Retourne le chemin du fichier
	 * 
	 * @return string Chemin du fichier
	**/
	public function getFilename() {
		return $this->stream->getFilename();
	}

	/**
	 * Retourne le token courant
	 *
	 * @return Token Le token courant
	 */
	public function getCurrentToken()
	{
		return $this->stream->current();
	}
}
