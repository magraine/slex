<?php

/**
 * L'AST est un arbre sémantique des données issues du parseur.
 *
 * Ce fichier recrée l'AST de SPIP <= 3.*
**/

namespace Spip\Compilo\Ast;

use Spip\Compilo\Ast;



/**
 * Ast, l'original, le vrai, l'historique
**/
class Original extends Ast
{
	/** Liste des boucles */
	protected $boucles;

	/** l'AST */
	protected $ast;

	/**
	 * Constructeur
	**/
	public function __construct() {
		$this->ast = new Original\Liste();
		$this->boucles = array();
	}


	/**
	 * Remplacer les noeuds
	 * @param Original\Liste $liste
	 */
	public function setNodes($liste) {
		if (!$liste instanceof Original\Liste) {
			throw new LogicException("Les enfants de l'ast Original doivent être de type Liste");
		}
		$this->ast = $liste;
	}


	/**
	 * Retourne l'AST
	 * @return array()
	**/
	public function getAst() {
		return $this->ast;
	}


	/**
	 * Retourne la liste des boucles classées par nom
	 * @return array()
	**/
	public function getBoucles() {
		return $this->boucles;
	}

	/**
	 * Retourne un noeud de Texte
	 * 
	 * @param string $texte Texte du texte
	 * @param string $avant Possible élément avant (apostrophe ?)
	 * @param string $apres Possible élément après (apostrophe ?)
	 * @param int $ligne Numéro de ligne
	 * @return Object
	**/
	public function Texte($texte, $avant, $apres, $ligne) {
		return new Original\Texte($texte, $avant, $apres, $ligne);
	}

	/**
	 * Retourne un noeud de Liste
	 * 
	 * @param array $noeuds Liste des éléments
	 * @param array $attributs Liste des attributs de la liste
	 * @param int $ligne Numéro de ligne
	 * @return Object
	**/
	public function Liste($noeuds, $attributs, $ligne) {
		return new Original\Liste($noeuds, $ligne);
	}

	/**
	 * Retourne un noeud de Polyglotte
	 * 
	 * @param array $noeuds Liste des éléments
	 * @param array $attributs Liste des attributs de la liste
	 * @param int $ligne Numéro de ligne
	 * @return Object
	**/
	public function Polyglotte($traductions, $ligne) {
		return new Original\Polyglotte($traductions, $ligne);
	}

	/**
	 * Retourne un noeud d'Idiome
	 * 
	 * @param array $module Module de langue
	 * @param array $cle Cle de langue
	 * @param int $ligne Numéro de ligne
	 * @return Object
	**/
	public function Idiome($module, $cle, $ligne) {
		return new Original\Idiome($module, $cle, $ligne);
	}
}
