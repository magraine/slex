<?php

/**
 * Noeud Texte
**/

namespace Spip\Compilo\Ast\Spici;

/**
 * Represente un noeud de texte
 */
class Texte extends Liste
{
	/**
	 * Constructeur
	 *
	 * @param string $texte
	 * @param int $lineno
	**/
	public function __construct($texte, $avant, $apres, $lineno)
	{
		parent::__construct(array(), array(
			'avant' => $apres,
			'texte' => $texte,
			'apres' => $apres,
		), array(), $lineno);
	}

	/**
	 * Compile le noeud
	 * @param Compiler
	 */
	public function compile(Compiler $compiler) {}
}
