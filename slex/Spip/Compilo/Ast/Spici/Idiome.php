<?php

/**
 * Noeud Texte
**/

namespace Spip\Compilo\Ast\Spici;

/**
 * Represente un noeud d'idiome
 */
class Idiome extends Liste
{
	/**
	 * Constructeur
	 *
	 * @param string $module
	 * @param string $cle
	 * @param int $lineno
	**/
	public function __construct($module, $cle, $lineno)
	{
		parent::__construct(array(), array(
			'module' => $module,
			'cle'    => $cle,
			#'arg'    => array(),
			#'param'  => array(),
		), array(), $lineno);
	}

	/**
	 * Compile le noeud
	 * @param Compiler
	 */
	public function compile(Compiler $compiler) {}
}
