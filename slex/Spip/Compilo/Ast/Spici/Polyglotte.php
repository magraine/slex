<?php

/**
 * Noeud Polyglotte
**/

namespace Spip\Compilo\Ast\Spici;

/**
 * Represente un noeud de Polyglotte
 */
class Polyglotte extends Liste
{

	/**
	 * Constructeur
	 *
	 * @param array $traductions
	 * @param int $lineno
	**/
	public function __construct($traductions, $lineno)
	{
		foreach ($traductions as $langue => $texte) {
			$traductions[$langue] = new Texte($texte,null,null,$lineno);
		}
		parent::__construct($traductions, array(), $lineno);
	}

	/**
	 * Compile le noeud
	 * @param Compiler
	 */
	public function compile(Compiler $compiler) {}
}
