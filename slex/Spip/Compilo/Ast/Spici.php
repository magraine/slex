<?php

/**
 * L'AST est un arbre sémantique des données issues du parseur.
 *
 * spic/ : radical esperanto pour Épice (prononcer spits)
 * 
 * - spici : épicer, assaisonner (prononcer spitsi)
 * - spico : épice
 * - spica : piquant, épicé 
 * 
 * Ce fichier crée un AST Spic
**/

namespace Spip\Compilo\Ast;

use Spip\Compilo\Ast;

/**
 * Ast
**/
class Spici extends Ast
{
	/** Liste des boucles */
	protected $boucles;

	/** l'AST */
	protected $ast;

	/**
	 * Retourne la liste des boucles classées par nom
	 * @return array()
	**/
	public function getBoucles() {
		return $this->boucles;
	}

	/**
	 * Retourne la liste des boucles classées par nom
	 * @return array()
	**/
	public function getAst() {
		return $this->ast;
	}


	/**
	 * Remplacer les noeuds
	 * @param Spici\Liste $liste
	 */
	public function setNodes($liste) {
		if (!$liste instanceof Spici\Liste) {
			throw new LogicException("Les enfants de l'ast Spici doivent être de type Liste");
		}
		$this->ast = $liste;
	}

	/**
	 * Retourne un noeud de Texte
	 * 
	 * @param string $texte Texte du texte
	 * @param string $avant Possible élément avant (apostrophe ?)
	 * @param string $apres Possible élément après (apostrophe ?)
	 * @param int $ligne Numéro de ligne
	 * @return Object
	**/
	public function Texte($texte, $avant, $apres, $ligne) {
		return new Spici\Texte($texte, $avant, $apres, $ligne);
	}

	/**
	 * Retourne un noeud de Liste
	 * 
	 * @param array $noeuds Liste des éléments
	 * @param array $attributs Liste des attributs de la liste
	 * @param int $ligne Numéro de ligne
	 * @return Object
	**/
	public function Liste($noeuds, $attributs, $ligne) {
		return new Spici\Liste($noeuds, $attributs, $ligne);
	}


	/**
	 * Retourne un noeud de Polyglotte
	 * 
	 * @param array $noeuds Liste des éléments
	 * @param array $attributs Liste des attributs de la liste
	 * @param int $ligne Numéro de ligne
	 * @return Object
	**/
	public function Polyglotte($traductions, $ligne) {
		return new Spici\Polyglotte($traductions, $ligne);
	}

	/**
	 * Retourne un noeud d'Idiome
	 * 
	 * @param array $module Module de langue
	 * @param array $cle Cle de langue
	 * @param int $ligne Numéro de ligne
	 * @return Object
	**/
	public function Idiome($module, $cle, $ligne) {
		return new Spici\Idiome($module, $cle, $ligne);
	}
}
