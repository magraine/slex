<?php

/**
 * Noeud Texte
**/

namespace Spip\Compilo\Ast\Original;


/**
 * Represente un noeud de texte
 */
class Texte extends Base {
	/**
	 * Type de noeud 
	 * @var string */
	public $type = 'texte';

	/**
	 * Le texte
	 * @var string */
	public $texte;

	/**
	 * Contenu avant le texte.
	 *
	 * Vide ou apostrophe simple ou double si le texte en était entouré
	 * @var string|array */
	public $avant = "";

	/**
	 * Contenu après le texte.
	 *
	 * Vide ou apostrophe simple ou double si le texte en était entouré
	 * @var string|array */
	public $apres = "";

	/**
	 * Numéro de ligne dans le code source du squelette
	 * @var int  */
	public $ligne = 0;


	/**
	 * Constructeur
	 *
	 * @param string $texte Texte du texte
	 * @param string $avant Possible élément avant
	 * @param string $apres Possible élément après
	 * @param int $ligne Numéro de ligne
	 */
	 public function __construct($texte=null, $avant="", $apres="", $ligne=0) {
		$this->texte = $texte;
		$this->avant = $avant;
		$this->apres = $apres;
		$this->ligne = $ligne;
	 }

	/**
	 * Conversion en texte
	**/
	public function __toString() {
		$nom = explode('\\', get_class($this));
		return #get_class($this)
			end($nom)
			. ' : ' . $this->avant . $this->texte . $this->apres;
	}

	/**
	 * Exporter en XML
	 *
	 * @param bool $asDom
	 *     Retourne ou non un DOMDocument, sinon une chaine XML
	 * @return DOMDocument|string
	 *     Texte XML ou objet DOMDocument
	**/
	public function toXml($asDom = false) {
		list($dom, $xml) = $this->startXML();

		$xml->appendChild($node = $dom->createElement('texte', $this->texte));
		#$node->setAttribute('class', get_class($this));
		if ($this->avant) {
			$node->setAttribute('avant', $this->avant);
		}
		if ($this->apres) {
			$node->setAttribute('apres', $this->apres);
		}

		return $asDom ? $dom : $dom->saveXml();
	}
}
