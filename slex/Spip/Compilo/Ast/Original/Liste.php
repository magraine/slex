<?php

/**
 * Une liste est un ensemble d'éléments de l'AST, dans l'ordre de leur apparition
 *
 * @note
 *    Ce code est grandement copié de la classe Twig_Node dans Twig 1.12.2
 *    Grands remerciements donc à Fabien Potencier et au projet Twig
**/

namespace Spip\Compilo\Ast\Original;


/**
 * Noeud de l'Ast
 *
 * On en fait une classe pour avoir quelques methodes utiles,
 * mais aussi un iterateur de tableau pour le faire comporter
 * comme si on avait un array(), comme le format d'origine de SPIP < 3.
 */
class Liste extends Base implements \Countable, \IteratorAggregate, \ArrayAccess
{
	/** les noeuds */
	protected $nodes;
	/** le numéro de ligne */
	#protected $lineno;
	/** nom de tag du noeud (pour export xml) */
	protected $tag;

	/**
	 * Constructeur.
	 *
	 * Les noeuds peuvent être automatiquement obtenus par propriété ($this->node).
	 * Les attributs, automatiquement comme élément de tableau ($this['name']).
	 *
	 * @param array   $nodes      Liste nommée de noeuds
	 * @param integer $lineno     Numéro de ligne
	 * @param string  $tag        Tag associé au noeud
	 */
	public function __construct(array $nodes = array(), $lineno = 0, $tag = null)
	{
		$this->nodes = $this->concatTexte($nodes);
		#$this->lineno = $lineno;
		$this->tag = $tag;
	}



	/**
	 * Concaténer des textes qui se suivent
	 *
	 * @param Node[] $nodes Liste des nodes
	**/
	private function concatTexte($nodes) {
		if (count($nodes) > 1) {
			$start = $texte = null;
			$n = 0;
			foreach ($nodes as $i => $node) {
				// on concatène les textes qui se suivent
				// si et seulement si ils n'ont pas de contenu définis avant ou après
				// (apostrophes)
				if (($node instanceof Texte)
				  and is_null($node->avant)
				  and is_null($node->apres))
				{
					if (is_null($start)) {
						$start = $i;
						$n = 0;
						$texte = $node->texte;
					} else {
						$n++;
						$texte .= $node->texte;
					}
				} else {

					// si on a commencé et qu'il y a au moins deux éléments consécutifs
					if ($n) {
						$nodes[$start]->texte = $texte;
						for ($j = $start+1; $j <= ($start+$n); $j++) {
							unset($nodes[$j]);
						}
					}

					// reinit
					$start = $texte = null;
					$n = 0;
				}
			}

			// pareil si on a terminé sur un texte concaténable
			if ($n) {
				$nodes[$start]->texte = $texte;
				for ($j = $start+1; $j <= ($start+$n); $j++) {
					unset($nodes[$j]);
				}
				$start = $texte = null;
			}
		}

		return $nodes;
	}




	/**
	 * Export en texte
	 *
	 * @return string
	**/
	public function __toString()
	{
		#$repr = array(get_class($this) . '(');
		$nom = explode('\\', get_class($this));
		$repr = array(end($nom) . '(');

		if (count($this->nodes)) {
			foreach ($this->nodes as $name => $node) {
				$len = strlen($name) + 4;
				$noderepr = array();
				foreach (explode("\n", (string) $node) as $line) {
					$noderepr[] = str_repeat(' ', $len).$line;
				}

				$repr[] = sprintf('  %s: %s', $name, ltrim(implode("\n", $noderepr)));
			}

			$repr[] = ')';
		} else {
			$repr[0] .= ')';
		}

		return implode("\n", $repr);
	}

	/**
	 * Exporter en XML
	 *
	 * @param bool $asDom
	 *     Retourne ou non un DOMDocument, sinon une chaine XML
	 * @return DOMDocument|string
	 *     Texte XML ou objet DOMDocument
	**/
	public function toXml($asDom = false)
	{
		list($dom, $xml) = $this->startXML();

		$xml->appendChild($node = $dom->createElement('liste'));
		#$node->setAttribute('class', get_class($this));

		foreach ($this->nodes as $name => $n) {
			if (null === $n) {
				continue;
			}

			$child = $n->toXml(true)->getElementsByTagName('spip')->item(0)->firstChild;
			$child = $dom->importNode($child, true);
			#$child->setAttribute('name', $name);

			$node->appendChild($child);

		}

		return $asDom ? $dom : $dom->saveXml();
	}


	/**
	 * Obtient le numéro de ligne
	 *
	 * @return int Numéro de ligne
	**/
	public function getLine() {
		return $this->lineno;
	}


	/**
	 * Compte le nombre de noeuds
	 *
	 * @return int Nombre de noeuds
	**/
	public function count() {
		return count($this->nodes);
	}


	/**
	 * Retourne l'itérateur sur les noeuds
	 *
	 * @return \ArrayIterator 
	**/
	public function getIterator() {
		return new \ArrayIterator($this->nodes);
	}


	/**
	 * Attribution d'une valeur
	 * 
	 * Ast[] = 'valeur';
	 *
	 * @param null|int|string $offset
	 * @param null|mixed $value
	**/
	public function offsetSet($offset, $value) {
		if (is_null($offset)) {
			$this->nodes[] = $value;
		} else {
			$this->nodes[$offset] = $value;
		}
	}

	/**
	 * Test d'existance d'une clé 
	 * @param int|string $offset
	**/
	public function offsetExists($offset) {
		return isset($this->nodes[$offset]);
	}

	/**
	 * Effacement d'une clé
	 * @param int|string $offset
	**/
	public function offsetUnset($offset) {
		unset($this->nodes[$offset]);
	}

	/**
	 * Obtention d'une clé
	 * @param int|string $offset
	 * @return mixed Valeur de la clé
	**/
	public function offsetGet($offset) {
		return isset($this->nodes[$offset]) ? $this->nodes[$offset] : null;
	}

}
