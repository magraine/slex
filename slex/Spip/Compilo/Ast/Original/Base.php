<?php

/**
 * Base des noeuds de l'ast Original
 *
 * Contient les fonctions communes
**/

namespace Spip\Compilo\Ast\Original;


/**
 * Base des noeuds de l'ast Original
 */
class Base {

	/**
	 * Type de noeud 
	 * @var string */
	protected $type_ast = 'Original';

	/**
	 * Prépare l'export XML en créant le document commun aux noeuds de l'arbre
	 *
	 * @return array(DomDocument, DomElement) Le XML, Son premier élément
	**/
	protected function startXML() {
		$dom = new \DOMDocument('1.0', 'UTF-8');
		$dom->formatOutput = true;
		$dom->appendChild($xml = $dom->createElement('spip'));
		$xml->setAttribute('type_ast', $this->type_ast);
		return array($dom, $xml);
	}
}
