<?php

/**
 * Noeud Polyglotte
**/

namespace Spip\Compilo\Ast\Original;



/**
 * Description d'un texte polyglotte (<multi>)
 *
 * @package SPIP\Core\Compilateur\AST
**/
class Polyglotte extends Base {
	/**
	 * Type de noeud 
	 * @var string */
	public $type = 'polyglotte';

	/**
	 * Tableau des traductions possibles classées par langue
	 *
	 * Tableau code de langue => texte
	 * @var array */
	var $traductions = array();

	/**
	 * Numéro de ligne dans le code source du squelette
	 * @var int */
	public $ligne = 0;



	/**
	 * Constructeur
	 *
	 * @param array $traductions
	 * @param int $lineno
	**/
	public function __construct($traductions, $lineno)
	{
		$this->traductions = $traductions;
		$this->ligne = $lineno;
	}

	/**
	 * Conversion en texte
	**/
	public function __toString() {
		$nom = explode('\\', get_class($this));
		$t = end($nom) . "(";
		if ($this->traductions) {
			$t .= "\n";
			foreach ($this->traductions as $langue => $texte) {
				$t .= "\t$langue" . str_repeat(" ", 8 - strlen($langue)) . ': ' . $texte . "\n";
			}
		}
		$t .= ")";
		return $t;
	}

	/**
	 * Exporter en XML
	 *
	 * @param bool $asDom
	 *     Retourne ou non un DOMDocument, sinon une chaine XML
	 * @return DOMDocument|string
	 *     Texte XML ou objet DOMDocument
	**/
	public function toXml($asDom = false) {
		list($dom, $xml) = $this->startXML();

		$xml->appendChild($node = $dom->createElement('polyglotte'));
		foreach ($this->traductions as $langue => $texte) {
			$node->appendChild($trad = $dom->createElement('traduction', $texte));
			$trad->setAttribute('langue', $langue);
		}

		return $asDom ? $dom : $dom->saveXml();
	}
}

