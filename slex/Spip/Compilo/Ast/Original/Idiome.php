<?php

/**
 * Noeud Texte
**/

namespace Spip\Compilo\Ast\Original;


/**
 * Represente un noeud de texte
 */
class Idiome extends Base {
	/**
	 * Type de noeud 
	 * @var string */
	public $type = 'idiome';

	/**
	 * Module
	 * @var string */
	public $module = "";

	/**
	 * Nom de la clé de langue
	 *
	 * @var string|array */
	public $nom_champ = "";

	/** à définir */
	public $arg   = array();
	public $param = array();

	/**
	 * Numéro de ligne dans le code source du squelette
	 * @var int  */
	public $ligne = 0;


	/**
	 * Constructeur
	 *
	 * @param string $module Nom du module de langue
	 * @param string $cle    Cle de langue
	 * @param int $ligne Numéro de ligne
	 */
	 public function __construct($module, $cle, $ligne=0) {
		$this->module    = $module;
		$this->nom_champ = $cle;
		$this->ligne     = $ligne;
	 }

	/**
	 * Conversion en texte
	**/
	public function __toString() {
		$nom = explode('\\', get_class($this));
		return #get_class($this)
			end($nom)
			. ' : ' . ($this->module ? $this->module . ' > ' : '') . $this->nom_champ;
	}

	/**
	 * Exporter en XML
	 *
	 * @param bool $asDom
	 *     Retourne ou non un DOMDocument, sinon une chaine XML
	 * @return DOMDocument|string
	 *     Texte XML ou objet DOMDocument
	**/
	public function toXml($asDom = false) {
		list($dom, $xml) = $this->startXML();

		$xml->appendChild($node = $dom->createElement('idiome', $this->nom_champ));
		#$node->setAttribute('class', get_class($this));
		if ($this->module) {
			$node->setAttribute('module', $this->module);
		}

		return $asDom ? $dom : $dom->saveXml();
	}
}
