<?php

/**
 * Autoloader des classes pour SPIP
**/

namespace SPIP;

/**
 * Autoloader
 *
 * @example
 *     ```
 *     require_once 'Spip/Autoloader.php';
 *     Spip\Autoloader::register();
 *     ```
 */
class Autoloader
{
	/**
	 * Déclare un autoloader pour SPL
	 */
	public static function register()
	{
		ini_set('unserialize_callback_func', 'spl_autoload_call');
		spl_autoload_register(array(new self, 'autoload'));
	}

	/**
	 * Chargement des classes
	 *
	 * @param string $class Un nom de classe
	 */
	public static function autoload($class)
	{
		if (0 !== strpos($class, 'Spip')) {
			return;
		}

		if (is_file($file = __DIR__.'/../'. str_replace('\\', '/' , $class) . '.php')) {
			require $file;
		}
	}
}
