<?php

/**
 * Prend un flux de tokens et crée une structure HTML
 * avec permettant de colorier un texte.
**/

namespace Spip\Helper;

use SPIP\Compilo\TokenStream;
use SPIP\Compilo\Token;

/**
 * Visualiseur HTML de tokens SPIP
**/
class TokenStreamViewer {

	/**
	 * Retourne une représentation HTML d'un flux de tokens
	 *
	 * @param TokenStream $stream  Flux de tokens SPIP
	 * @return string Code HTML
	**/
	public static function html(TokenStream $stream) {
		$html = "<pre class='spip_tokens'>";
		foreach ($stream as $token) {

			$v  = htmlspecialchars($token->getValue());
			$t  = $token->getType();
			$av = $token->getAvant();
			$ap = $token->getApres();

			switch ($t) {
				#case Token::ST_ECHAPPEMENT:
				#	$html .= "<span class='st_echappement_cle'>$e</span>"
				#			. "<span class='st_echappement'>" . $v . "</span>";  
				#	break;
				default:
					$t = strtolower(Token::typeToString($t, true));
					if ($av) {
						$html .= "<span class='$t avant'>" . $av . "</span>";
					}

					$html .= "<span class='$t'>" . $v . "</span>";

					if ($ap) {
						$html .= "<span class='$t apres'>" . $ap . "</span>";
					}
					break;
			}
		}
		$html .= "</pre>";
		return $html;
	}
}
