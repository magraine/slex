<?php

require_once __DIR__ . '/../../../Spip/Autoloader.php';
Spip\Autoloader::register();

use Spip\Compilo\Lexer;
use Spip\Compilo\TokenStream;
use Spip\Compilo\Token;
use PHPUnit\Framework\TestCase;


class LexerSpicoEchappementsTest extends TestCase
{

    public function testTokenizeEchappement()
    {
        $lexer = new Lexer\Spico();
        $stream = $lexer->tokenize("\\[ Echappe");
        $this->assertTrue($stream->test(Token::ST_ECHAPPEMENT, "["));
        $c = $stream->current();
        $this->assertEquals($c->getAvant(), "\\");
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, " Echappe"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_EOF));
    }

    public function testTokenizeEchappementAvecTexteAvant()
    {
        $lexer = new Lexer\Spico();
        $stream = $lexer->tokenize("Avant \\[ Echappe");
        $this->assertTrue($stream->test(Token::ST_TEXTE, "Avant "));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_ECHAPPEMENT, "["));
        $c = $stream->current();
        $this->assertEquals($c->getAvant(), "\\");
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, " Echappe"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_EOF));
    }

    public function testTokenizeEchappements()
    {
        $lexer = new Lexer\Spico();
        $stream = $lexer->tokenize("\\[\\]\\{\\}\\(\\)\\<\\>\\#");
        $this->assertTrue($stream->test(Token::ST_ECHAPPEMENT, "["));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_ECHAPPEMENT, "]"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_ECHAPPEMENT, "{"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_ECHAPPEMENT, "}"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_ECHAPPEMENT, "("));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_ECHAPPEMENT, ")"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_ECHAPPEMENT, "<"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_ECHAPPEMENT, ">"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_ECHAPPEMENT, "#"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_EOF));
    }
}

