<?php

require_once __DIR__ . '/../../../Spip/Autoloader.php';
Spip\Autoloader::register();

use Spip\Compilo\Lexer;
use Spip\Compilo\TokenStream;
use Spip\Compilo\Token;
use PHPUnit\Framework\TestCase;


class LexerSpicoCommentairesTest extends TestCase
{

    public function testTokenizeCommentaire()
    {
        $lexer = new Lexer\Spico();
        $stream = $lexer->tokenize("[% Commentaire %]");
        $this->assertTrue($stream->test(Token::ST_COMMENTAIRE, "[% Commentaire %]"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_EOF));
    }

    /**
     * @expectedException Spip\Compilo\Error\Syntax
     */
    public function testTokenizeCommentaireSansFin()
    {
        $lexer = new Lexer\Spico();
        $stream = $lexer->tokenize("[% Commentaire");
    }

    public function testTokenizeCommentaireDoc()
    {
        $lexer = new Lexer\Spico();
        $stream = $lexer->tokenize("[%% Commentaire %]");
        $this->assertTrue($stream->test(Token::ST_COMMENTAIRE_DOC, "[%% Commentaire %]"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_EOF));
    }

    public function testTokenizeEchappementEtCommentaire()
    {
        $lexer = new Lexer\Spico();
        $stream = $lexer->tokenize("\\[% Echappe");
        $this->assertTrue($stream->test(Token::ST_ECHAPPEMENT, "["));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, "% Echappe"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_EOF));
    }

    public function testTokenizeCommentairesDoubles()
    {
        $lexer = new Lexer\Spico();
        $stream = $lexer->tokenize("un [% commentaire %] deux [% commentaires %]");
        $this->assertTrue($stream->test(Token::ST_TEXTE, "un "));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_COMMENTAIRE, "[% commentaire %]"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, " deux "));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_COMMENTAIRE, "[% commentaires %]"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_EOF));
    }
}

