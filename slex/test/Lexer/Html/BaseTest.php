<?php

require_once __DIR__ . '/../../../Spip/Autoloader.php';
Spip\Autoloader::register();

use Spip\Compilo\Lexer;
use Spip\Compilo\TokenStream;
use Spip\Compilo\Token;
use PHPUnit\Framework\TestCase;


class LexerHtmlBaseTest extends TestCase
{

    public function testCodeDeDebutDePosition()
    {
        $this->assertEquals(Lexer\Html::positionCodeToString(Lexer\Html::DEBUT_COMMENTAIRE), 'Lexer::DEBUT_COMMENTAIRE');
        $this->assertEquals(Lexer\Html::positionCodeToString(Lexer\Html::DEBUT_COMMENTAIRE, true), 'DEBUT_COMMENTAIRE');
    }

    public function testTokenizeEmpty()
    {
        $lexer = new Lexer\Html();
        $stream = $lexer->tokenize("");
        # $this->assertInstanceOf("Spip\Compilo\TokenStream", $t);
        # le premier est un token de fin !
        $this->assertTrue($stream->test(Token::ST_EOF));
    }

    public function testTokenizeSimpleTexte()
    {
        $lexer = new Lexer\Html();
        $stream = $lexer->tokenize("SimpleTexte");
        $this->assertTrue($stream->test(Token::ST_TEXTE, "SimpleTexte"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_EOF));
    }

}

