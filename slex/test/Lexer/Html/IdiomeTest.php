<?php

require_once __DIR__ . '/../../../Spip/Autoloader.php';
Spip\Autoloader::register();

use Spip\Compilo\Lexer;
use Spip\Compilo\TokenStream;
use Spip\Compilo\Token;
use PHPUnit\Framework\TestCase;


class LexerHtmlIdiomesTest extends TestCase
{

    public function testTokenizeIdiome()
    {
        $lexer = new Lexer\Html();
        $stream = $lexer->tokenize("<:cle_de_langue:>");
        $this->assertTrue($stream->test(Token::ST_IDIOME_DEBUT, "<:"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_IDIOME_CLE, "cle_de_langue"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_IDIOME_FIN, ":>"));
    }

    public function testTokenizeIdiomeAvecModule()
    {
        $lexer = new Lexer\Html();
        $stream = $lexer->tokenize("<:module:cle_de_langue:>");
        $this->assertTrue($stream->test(Token::ST_IDIOME_DEBUT, "<:"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_IDIOME_MODULE, "module"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_IDIOME_CLE, "cle_de_langue"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_IDIOME_FIN, ":>"));
    }

    public function testTokenizeIdiomeAvecTexteAvant()
    {
        $lexer = new Lexer\Html();
        $stream = $lexer->tokenize("Texte <:cle_de_langue:>");
        $this->assertTrue($stream->test(Token::ST_TEXTE, "Texte "));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_IDIOME_DEBUT, "<:"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_IDIOME_CLE, "cle_de_langue"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_IDIOME_FIN, ":>"));
    }

    public function testTokenizeIdiomeAvecTexteApres()
    {
        $lexer = new Lexer\Html();
        $stream = $lexer->tokenize("<:cle_de_langue:> Texte");
        $this->assertTrue($stream->test(Token::ST_IDIOME_DEBUT, "<:"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_IDIOME_CLE, "cle_de_langue"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_IDIOME_FIN, ":>"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, " Texte"));
    }

    public function testTokenizeIdiomeDoubles()
    {
        $lexer = new Lexer\Html();
        $stream = $lexer->tokenize("<:idiome1:><:idiome2:>");
        $this->assertTrue($stream->test(Token::ST_IDIOME_DEBUT, "<:"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_IDIOME_CLE, "idiome1"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_IDIOME_FIN, ":>"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_IDIOME_DEBUT, "<:"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_IDIOME_CLE, "idiome2"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_IDIOME_FIN, ":>"));
        $stream->next();
    }

    public function testTokenizeIdiomeDoublesAvecTextes()
    {
        $lexer = new Lexer\Html();
        $stream = $lexer->tokenize("Un <:idiome1:> Deux <:idiome2:> Trois");
        $this->assertTrue($stream->test(Token::ST_TEXTE, "Un "));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_IDIOME_DEBUT, "<:"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_IDIOME_CLE, "idiome1"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_IDIOME_FIN, ":>"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, " Deux "));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_IDIOME_DEBUT, "<:"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_IDIOME_CLE, "idiome2"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_IDIOME_FIN, ":>"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, " Trois"));
    }
}

