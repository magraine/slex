<?php

require_once __DIR__ . '/../../../Spip/Autoloader.php';
Spip\Autoloader::register();

use Spip\Compilo\Lexer;
use Spip\Compilo\TokenStream;
use Spip\Compilo\Token;
use PHPUnit\Framework\TestCase;


class LexerHtmlPolyglottesTest extends TestCase
{

    public function testTokenizePolyglotte()
    {
        $lexer = new Lexer\Html();
        $stream = $lexer->tokenize("<multi>Texte</multi>");
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_DEBUT, "<multi>"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, "Texte"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_FIN, "</multi>"));
    }

    public function testTokenizePolyglotteAvecLangue()
    {
        $lexer = new Lexer\Html();
        $stream = $lexer->tokenize("<multi>[fr]Texte</multi>");
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_DEBUT, "<multi>"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_LANGUE, "fr"));
        $c = $stream->current();
        $this->assertEquals($c->getAvant(), "[");
        $this->assertEquals($c->getApres(), "]");
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, "Texte"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_FIN, "</multi>"));
    }

    /**
     * @depends testTokenizePolyglotteAvecLangue
    **/
    public function testTokenizePolyglotteAvecLangues()
    {
        $lexer = new Lexer\Html();
        $stream = $lexer->tokenize("<multi>[fr]Texte[en]Text</multi>");
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_DEBUT, "<multi>"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_LANGUE, "fr"));
        $c = $stream->current();
        $this->assertEquals($c->getAvant(), "[");
        $this->assertEquals($c->getApres(), "]");
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, "Texte"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_LANGUE, "en"));
        $c = $stream->current();
        $this->assertEquals($c->getAvant(), "[");
        $this->assertEquals($c->getApres(), "]");
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, "Text"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_FIN, "</multi>"));
    }


    /**
     * @depends testTokenizePolyglotteAvecLangues
    **/
    public function testTokenizePolyglotteAvecLanguesEtTexteAvant()
    {
        $lexer = new Lexer\Html();
        $stream = $lexer->tokenize("<multi>Avant[fr]Texte[en]Text</multi>");
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_DEBUT, "<multi>"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, "Avant"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_LANGUE, "fr"));
        $c = $stream->current();
        $this->assertEquals($c->getAvant(), "[");
        $this->assertEquals($c->getApres(), "]");
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, "Texte"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_LANGUE, "en"));
        $c = $stream->current();
        $this->assertEquals($c->getAvant(), "[");
        $this->assertEquals($c->getApres(), "]");
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, "Text"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_FIN, "</multi>"));
    }


    /**
     * @depends testTokenizePolyglotte
    **/
    public function testTokenizePolyglotteDansTexte()
    {
        $lexer = new Lexer\Html();
        $stream = $lexer->tokenize("Avant <multi> Texte </multi> Apres");
        $this->assertTrue($stream->test(Token::ST_TEXTE, "Avant "));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_DEBUT, "<multi>"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, " Texte "));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_FIN, "</multi>"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, " Apres"));
    }


    /**
     * Ce test ne doit pas se planter sur le [ qui se promène
     * 
     * @depends testTokenizePolyglotteAvecLangues
    **/
    public function testTokenizePolyglotteAvecCrochet()
    {
        $lexer = new Lexer\Html();
        $stream = $lexer->tokenize("<multi>[en]Text [ Text [fr]Texte</multi>");
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_DEBUT, "<multi>"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_LANGUE, "en"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, "Text [ Text "));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_LANGUE, "fr"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, "Texte"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_FIN, "</multi>"));
    }



    /**
     * Ce test ne doit pas rendre le commentaire, vu que ce lexeur ne les gère pas
     * 
     * @depends testTokenizePolyglotteAvecLangues
    **/
    public function testTokenizePolyglotteAvecLanguesEtCommentaires()
    {
        $lexer = new Lexer\Html();
        $stream = $lexer->tokenize("<multi>[en]Text[% [fr]Texte %] apres[eo]Teksto</multi>");
       # var_dump($stream);
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_DEBUT, "<multi>"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_LANGUE, "en"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, "Text[% "));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_LANGUE, "fr"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, "Texte %] apres"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_LANGUE, "eo"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, "Teksto"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_FIN, "</multi>"));
    }



    /**
     * Ce test ne doit pas rendre le commentaire, vu que ce lexeur ne les gère pas
     * 
     * @depends testTokenizePolyglotteAvecLangues
    **/
    public function testTokenizePolyglotteAvecLanguesEtCommentairesEtMultiFerme()
    {
        $lexer = new Lexer\Html();
        $stream = $lexer->tokenize("<multi>Text[% </multi> %]Apres</multi>");
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_DEBUT, "<multi>"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, "Text[% "));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_FIN, "</multi>"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, " %]Apres</multi>"));
    }



    /**
     * @depends testTokenizePolyglotte
    **/
    public function testTokenizePolyglotteAEtEchappement()
    {
        $lexer = new Lexer\Html();
        $stream = $lexer->tokenize("<multi>Texte \[en\] Apres</multi>");
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_DEBUT, "<multi>"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, "Texte "));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_ECHAPPEMENT, "["));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, "en"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_ECHAPPEMENT, "]"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, " Apres"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_FIN, "</multi>"));
    }

    /**
     * @depends testTokenizePolyglotte
    **/
    public function testTokenizePolyglotteDoubles()
    {
        $lexer = new Lexer\Spico();
        $stream = $lexer->tokenize("un <multi>[fr]Texte1</multi> deux <multi>[fr]Texte2</multi> trois");
        $this->assertTrue($stream->test(Token::ST_TEXTE, "un "));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_DEBUT, "<multi>"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_LANGUE, "fr"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, "Texte1"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_FIN, "</multi>"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, " deux "));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_DEBUT, "<multi>"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_LANGUE, "fr"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, "Texte2"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_POLYGLOTTE_FIN, "</multi>"));
        $stream->next();
        $this->assertTrue($stream->test(Token::ST_TEXTE, " trois"));
    }
}

