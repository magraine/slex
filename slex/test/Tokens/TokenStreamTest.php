<?php

require_once __DIR__ . '/../../Spip/Autoloader.php';
Spip\Autoloader::register();


use Spip\Compilo\Token;
use Spip\Compilo\TokenStream;
use PHPUnit\Framework\TestCase;


class TokenStreamTest extends TestCase
{

    public function testNext()
    {
        $stream = new TokenStream($this->getTokens());
        $this->assertEquals($stream->current(), new Token(Token::ST_TEXTE,'texte', 1));
        $stream->next();
        $this->assertEquals($stream->current(), new Token(Token::ST_COMMENTAIRE,'[% texte %]', 2));
        $stream->next();
        $this->assertEquals($stream->current(), new Token(Token::ST_EOF, '', 3));
    }


    private function getTokens() {
        return array(
            new Token(Token::ST_TEXTE,'texte', 1),
            new Token(Token::ST_COMMENTAIRE,'[% texte %]', 2),
            new Token(Token::ST_EOF, '', 3),
        );
    }


}

