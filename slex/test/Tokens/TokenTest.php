<?php

require_once __DIR__ . '/../../Spip/Autoloader.php';
Spip\Autoloader::register();


use Spip\Compilo\Token;
use PhpUnit\Framework\TestCase;


class TokenTest extends TestCase
{

    public function testGets()
    {
        $token = new Token(Token::ST_TEXTE, 'texte', 1);
        $this->assertEquals($token->getType(), Token::ST_TEXTE);
        $this->assertEquals($token->getValue(), 'texte');
        $this->assertEquals($token->getLine(), 1);
    }


    public function testTest()
    {
        $token = new Token(Token::ST_TEXTE, 'texte', 1);
        $this->assertTrue($token->test(Token::ST_TEXTE));
        $this->assertTrue($token->test(Token::ST_TEXTE, 'texte'));
        $this->assertFalse($token->test(Token::ST_COMMENTAIRE));
    }

    public function testType()
    {
        $this->assertEquals(Token::typeToString(Token::ST_TEXTE), 'Token::ST_TEXTE');
        $this->assertEquals(Token::typeToString(Token::ST_TEXTE, true), 'ST_TEXTE');
        $this->assertEquals(Token::typeToString(Token::ST_COMMENTAIRE), 'Token::ST_COMMENTAIRE');
        $this->assertEquals(Token::typeToString(Token::ST_COMMENTAIRE, true), 'ST_COMMENTAIRE');
    }
}

